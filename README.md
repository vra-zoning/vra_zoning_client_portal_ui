## VRA_Zoning_Client_Portal_UI Project

### Functionalities
- This project is responsible for all the UI code present in the client portal 
-  Site summary
-   Zoning
-  Landscaping
- Nearby restaurants
-  VRALite
-  Login
-  Registration
-  VRA subscriptions
-  Admin portal
 
### Setup in development/local system
- Clone the repo in your local system  
-  Navigate to the root directory  
- **npm install**  
-  **npm start**  

*Note:* Make sure that the webservices are running in the backend

### Tech stack
- React v18
- Html 
- CSS
