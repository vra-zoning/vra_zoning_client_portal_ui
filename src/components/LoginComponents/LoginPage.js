import React, { useState } from "react";
import { useNavigate } from 'react-router-dom';
import "./LoginPage.css";
import { Visibility, VisibilityOff } from '@mui/icons-material';
import axios from "axios";

/* eslint-disable no-useless-escape */
const LoginPage = () => {

  const API_Endpoint ="https://vrazoningclientws.azurewebsites.net"

  const [email, setEmail] = useState("");
  const [showErr, setShowErr] = useState(false);
  const [emailErr, setEmailErr] = useState(false);
  const [password, setPassword] = useState("");
  const [hidePassword, setHidePassword] = useState(true);
  const [invalidEmail, setInvalidEmail] = useState(false);

  var regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  const navigate = useNavigate();
  localStorage.getItem("user") && navigate("/");

  const onClientlogin = () => {
    const body = {
      "email": email,
      "password": password
    };

    //check with regex for correct email
    checkEmail();

    //first validate email if it already exists in the DB
    !invalidEmail && emailExists(body);
  }

  const validate = (validateEmail) => {
    const body = {
      "email": email,
      "password": password
    };
    if (validateEmail) {
      axios.post(`${API_Endpoint}/validate`, body,
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
        })
        .then(function (response) {
          if (response && response.data) {
            const modData = { ...response.data };
            modData.cust_id = response.data.cusT_ID ? response.data.cusT_ID : response.data.cust_id;
            response.data.login = Date.now();
            localStorage.setItem("user_unsigned", JSON.stringify(response.data));
            navigate("/DualAuth");
          }
          if (response.status === 204) {
            setShowErr(true);
          }
        })
        .catch(function (error) {
          console.log("err", error);
        });
    }
    //if email does not exist, prompt user to register first
    else {
    }
  }

  const emailExists = async (data) => {
    await axios.post(`${API_Endpoint}/validateEmail`, data,
      {
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      })
      .then(function (response) {
        if (response && response.data) {
          validate(true);
        }
        if (response.status === 204) {
          validate(false);
          window.alert("email does not exist, register to create a new account")
        }
      })
      .catch(function (error) {
        console.log("err", error);
      });

  }

  const onForgotPassword = async () => {
    if (!email || (!(regex.test(email) === true))) {
      setEmailErr(true)
      window.alert("enter valid email")
    }
    else {
      await axios.get(`${API_Endpoint}/forgotPass/${email}`,
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
        })
      window.alert("Password rest link sent to the email provided!")
    }
  }

  const checkEmail = () => {
    if (!(regex.test(email) === true)) {
      window.alert("Enter valid email")
      setInvalidEmail(true);
    }
    else {
      setInvalidEmail(false)
    }
  }

  return (
    <div className="main-div">
      <div className="left">
        Please Login
        <div className="each-input" >
          <input
            type="text"
            value={email}
            placeholder="Enter Your Email"
            style={showErr || emailErr ? { borderColor: "red" } : {}}
            onFocus={() => { setShowErr(false); setEmailErr() }}
            onChange={(e) => setEmail(e.target.value)}
            onKeyDown={(e) => { (e.which === 13 || e.keyCode === 13) && onClientlogin() }}
          />
          <a href="# " className="forgot-password" onClick={onForgotPassword}>Forgot password?</a>
          <div className="password-block-login">
            <input
              type={hidePassword ? "password" : "text"}
              value={password}
              placeholder="Enter Your Password"
              style={showErr ? { borderColor: "red" } : {}}
              onFocus={() => setShowErr(false)}
              onChange={(e) => setPassword(e.target.value)}
              onKeyDown={(e) => { (e.which === 13 || e.keyCode === 13) && onClientlogin() }}
            />
            {hidePassword ? <Visibility onClick={() => setHidePassword(false)} /> : <VisibilityOff onClick={() => setHidePassword(true)} />}
          </div>
          {showErr && <div style={{ color: "white", fontWeight: "400", fontSize: "16px" }}>Email/Password do not match!</div>}
          <button className="app-btn login-btn" onClick={onClientlogin}>Login</button>
        </div>
      </div>
      <div className="right">
        <div className="logo-heading">
          <img alt="Logo not found" src={'../images/vra_logo.png'} style={{ height: "100px", borderRadius: "10px", background: "whitesmoke" }} />
        </div>
        <br />
        <div className="textLogin sub-head">Expedite your Planning and Zoning Review with VRA</div>
        <br />
        <div className="textLogin" style={{ paddingRight: "15%", paddingLeft: "15%" }}>
          A comprehensive plan review that examines your plans in seconds</div>
        <button className="app-btn" onClick={() => navigate("/register")}>Register</button>
      </div>
    </div>
  );
};

export default LoginPage;
