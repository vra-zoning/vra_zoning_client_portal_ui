import React, { useState, useEffect } from "react";
import { Link, useNavigate } from 'react-router-dom';
import { Formik, Field, Form } from 'formik';
import axios from "axios";
import { Visibility, VisibilityOff } from '@mui/icons-material';
import Modal from "./../Modal.js";
import "./ResetPassword.css";

const ResetPassword = (props) => {

    const API_Endpoint ="https://vrazoningclientws.azurewebsites.net"

    const [form, setForm] = useState({ password: "", confirmPassword: "" });
    const [loading, setLoading] = useState(true);
    const { password, confirmPassword } = form;
    const navigate = useNavigate();
    const [email, setEmail] = useState("");
    const [hidePassword, setHidePassword] = useState(true);
    const [hideConfirmPassword, setHideConfirmPassword] = useState(true);
    const [showPassErr, setShowPassErr] = useState(false);
    const [showConPassErr, setShowConPassErr] = useState(false);


    const onSubmit = async () => {
        if (!showConPassErr && !showConPassErr) {
            const body = { password }
            const response = await axios.put(`${API_Endpoint}/api/customers/${email}`, body,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                    },
                })
            if (response.status === 200) {
                navigate("/login");
            }
        }
    }

    const checkValidation = (pw) => {
        //check empty password field
        const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,20}$/g;
        return regex.test(pw);
    }

    const onPassChange = (e) => {
        const value = e.target.value
        const name = e.target.name
        const pass = name === "password" ? value : password;
        const conPass = name === "confirmPassword" ? value : confirmPassword;
        let passErr = false, conPassErr = false;
        if (pass && !checkValidation(pass)) passErr = true;
        if (conPass && !checkValidation(conPass)) conPassErr = true;
        if (conPass && pass && !passErr && !conPassErr && pass !== conPass) { passErr = true; conPassErr = true; }
        setShowPassErr(passErr);
        setShowConPassErr(conPassErr);
        setForm({ password: pass, confirmPassword: conPass });
    }

    useEffect(() => {
        (async () => {
            // remove token from url to prevent http referer leakage
            const currentPath = window.location.pathname;
            let token = "";
            if (currentPath.includes("/reset-password/")) {
                token = currentPath.replace("/reset-password/", "");
                navigate(window.location.pathname, { replace: true })
                const body = { "password": token }
                try {
                    const response = await axios.post(`${API_Endpoint}/validateToken`, body,
                        {
                            headers: {
                                'Content-Type': 'application/json',
                                'Accept': 'application/json',
                            },
                        })
                    if (response.status === 200) {
                        setLoading(false);
                        setEmail(response.data)
                    } else {
                        navigate("/");
                    }
                } catch (err) { navigate("/"); };
            }
        })();
    }, [])

    const initialValues = {
        password: '',
        confirmPassword: '',
    };

    return <div>
        <link rel="stylesheet" href="/css/bootstrap.min.css" />
        <div className="app-container" >
            <Modal show handleClose={(e) => props.modalClose(e)}>
                <div className="container">
                    <div className="row" style={{ marginTop: "10%" }} >
                        <div className="col-sm-6 offset-sm-2 mt-5">
                            <div className="card m-5">
                                <h3 className="card-header">Reset Password</h3>
                                <div className="card-body">
                                    {loading ? "loading..." : <Formik initialValues={initialValues} onSubmit={onSubmit}>
                                        {({ errors, touched, isSubmitting }) => (
                                            <Form>
                                                <div className="form-group">
                                                    <label>Password</label>
                                                    <div className="password-block">
                                                        <Field
                                                            name="password"
                                                            type={hidePassword ? "password" : "text"}
                                                            className={'form-control' + (showPassErr ? ' pass-error' : '')}
                                                            value={password}
                                                            onChange={onPassChange}
                                                        />
                                                        {hidePassword ? <Visibility onClick={() => setHidePassword(false)} /> : <VisibilityOff onClick={() => setHidePassword(true)} />}
                                                    </div>
                                                </div>
                                                <div className="form-group">
                                                    <label>Confirm Password</label>
                                                    <div className="password-block">
                                                        <Field
                                                            name="confirmPassword"
                                                            type={hideConfirmPassword ? "password" : "text"}
                                                            className={'form-control' + (showConPassErr ? ' pass-error' : '')}
                                                            value={confirmPassword}
                                                            onChange={onPassChange}
                                                        />
                                                        {hideConfirmPassword ? <Visibility onClick={() => setHideConfirmPassword(false)} /> : <VisibilityOff onClick={() => setHideConfirmPassword(true)} />}
                                                    </div>
                                                </div>
                                                <div className={`password-warning ${showPassErr || showConPassErr ? "warning-error" : ""}`}>Password should contain 8-20 characters, 1 uppercase, 1 lowercase, 1 special character, 1 number</div>
                                                <div className="form-row">
                                                    <div className="form-group col" style={{ marginTop: "5%" }}>
                                                        <button type="submit" disabled={isSubmitting} className="btn btn-primary">
                                                            {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                                                            Reset Password
                                                        </button>
                                                        <Link to="login" className="btn btn-link" onClick={() => navigate("/login")}>Cancel</Link>
                                                    </div>
                                                </div>
                                            </Form>
                                        )}
                                    </Formik>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Modal>
        </div>
    </div>
}

export default ResetPassword;