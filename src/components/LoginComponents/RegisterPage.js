import React, { useState } from "react";
import "./RegisterPage.css";
import axios from "axios";
import { useNavigate } from 'react-router-dom';
import { passwordStrength } from 'check-password-strength';
import { Visibility, VisibilityOff } from '@mui/icons-material';
import Dropdown from "./Dropdown.js";
import { sendVerificationEmailNotification } from "../emailNotification.js";

const RegisterPage = () => {

    const API_Endpoint ="https://vrazoningclientws.azurewebsites.net"

    const [formData, setFormData] = useState({});
    const [errorMessage, setErrorMessage] = useState("");
    const [emailError, setEmailError] = useState(false);
    const [code, setCode] = useState("");
    const navigate = useNavigate();
    const [disabledCode, setDisabledCode] = useState(false);
    const [validPhone, setValidPhone] = useState('');
    const [validPassword, setValidPassword] = useState('');
    const [showPassword, setShowPassword] = useState(false);
    const [licenseType, setlicenseType] = useState("Standard");
    const [subType, setSubType] = useState("onemonth");

    localStorage.getItem("user") && navigate("/");

    const validateEmail = (email) => {
        return String(email)
            .toLowerCase()
            .match(
                /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            );
    }

    const onRegisterClick = async (event) => {

        event.preventDefault();
        const { email, firstName, middleName, password, lastName, address, phone } = formData;
        // var custLicenseInfo;
        if (validateEmail(formData.email)) {
            let body_2;
            const data ={
                "email":email
            }

            await axios.post(`${API_Endpoint}/validateEmail`, data,
            {
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
              },
            })
            .then(async function (response) {
              
              if (response.status === 200) {
                 window.alert("email already Exist. You can Use Forget Password!!!")
                 navigate("/login");
              }else{
                if (response.status === 204){
                const body = {
                    "email": email,
                    "password": password,
                    "firstName": firstName,
                    "middleName": middleName,
                    "lastName": lastName,
                    "mailingAddress": address,
                    "mobile": phone,
                    "registered_on": new Date().toISOString(),
                };
                const response = await axios.post(`${API_Endpoint}/api/customers`, body,
                    {
                        headers: {
                            'Content-Type': 'application/json',
                            'Accept': 'application/json',
                        },
                    });
                    // .then(function (response) {
                    if (response && response.data && !response.data.status) {
                        // localStorage.setItem("user", JSON.stringify(data));
                        navigate("/login");
                        body_2 = {
                            "email": email,
                            "password": password,
                            "firstName": firstName,
                            "middleName": middleName,
                            "lastName": lastName,
                            "mailingAddress": address,
                            "mobile": phone,
                            "RegisteredTime": Date.now(),
                            "license_type": licenseType,
                            "subscription_type": subType,
                            "customer_Id": response.data.cusT_ID? response.data.cusT_ID: response.data.cust_id
                        };
        
                    }
                    if (response.data && response.data.status) {
                        setErrorMessage(response.data.message);;
                    }
                    var CryptoJS = require("crypto-js");
                    //encrpting the data body_2
                    let en_Userdetails = CryptoJS.AES.encrypt(JSON.stringify(body_2), "vrazoning")
                    let approvallink = "https://vrazoningreportui.azurewebsites.net/validate/" + en_Userdetails.toString();
                    console.log(approvallink);
                    //sending a encrpted link to Approval mail function.
                    Approvalmail(body, approvallink)
              }
            }})
            .catch(function (error) {
              console.log("err", error);
            });
            
            
        } else setEmailError(true);
    };

    const fields = [
        { value: "First Name*", saveValue: "firstName" },
        { value: "Middle Name", saveValue: "middleName" },
        { value: "Last Name*", saveValue: "lastName" },
        { value: "Email Address*", saveValue: "email" },
        { value: "Password*", saveValue: "password" },
        { value: "Verify Email Code*", saveValue: "code" },
        { value: "Phone Number*", saveValue: "phone" },
        { value: "Mailing Address*", saveValue: "address" }
    ];

    const setValue = (e, field) => {
        const data = { ...formData }
        data[field.saveValue] = e.target.value;
        setFormData(data);
    }

    const getRandomInt = (min, max) => {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min) + min);
    }

    const sendCode = (e) => {
        e.preventDefault();
        const code = getRandomInt(100000, 999999);
        setCode(code);
        const templateParams = {
            from_name: "VRA Client Verification",
            to_name: formData.firstName,
            subject: "VRA Client Verification",
            user_name: "VRA",
            message1: `Your Email Verification Code is ${code}. Please enter this code on the VRA Registration Page, to continue.`,
            cc_candidates: `${formData.email}`
        };
        setDisabledCode(true);
        sendVerificationEmailNotification(templateParams, () => { setTimeout(() => { setDisabledCode(false) }, 3000) });
    }


    const Approvalmail = (e, approvallink) => {
        console.log("approval main function called.")
        const templateParams = {
            from_name: formData.firstName,
            to_name: "VRA TEAM ",
            subject: "VRA Client Need Verification",
            user_name: "VRA",
            message1: "Click here For Approval:" + approvallink,
            cc_candidates: `raviteja.lanka@ufl.edu;`
            // cc_candidates:"s.madipadige@virtualreviewassist.com;niteesh.beeredd@ufl.edu;pz@virtualreviewassist.com"
        };
        sendVerificationEmailNotification(templateParams, () => { });
    }


    const onInputBlur = (field) => {
        console.log(field, validPhone);
        if (field.saveValue === 'phone') {
            const regex = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/g;
            setValidPhone(regex.test(formData[field.saveValue]));
        }
        if (field.saveValue === 'password') {
            const ps = passwordStrength(formData[field.saveValue]);
            setValidPassword(ps.contains.length >= 4 && ps.length >= 8 && ps.length <= 20);
        }
    }


    //variables to set license type
    const handleLicenseTypeChange = (event) => {
        setlicenseType(event.target.value);
    };

    //variables to set subscription type
    const handleSubTypeChange = (event) => {
        setSubType(event.target.value);
    };

    const disabled = !(formData.firstName && formData.lastName && formData.email && validPassword && formData.address && validPhone && formData.code === code.toString());
    return (
        <div className="registerPage">
            <link rel="stylesheet" href="/css/bootstrap.min.css" />
            <div className="container2">
                <div className="heading">
                    <header>Registration</header>
                </div>
                <div className="form-outer1">
                    <form>
                        <div className="register-page slide-page">
                            {fields.map((field, _) => <div className="field">
                                <div className="label">
                                    {field.value}
                                </div>
                                <input
                                    className={`${field.saveValue === "email" ? "email-length " : ""}${field.saveValue === "email" && emailError ? "err" : ""}`}
                                    onFocus={() => field.saveValue === "email" && emailError && setEmailError(false)}
                                    type={field.saveValue === 'password' && !showPassword ? "password" : "text"}
                                    onBlur={() => onInputBlur(field)}
                                    onChange={(e) => setValue(e, field)}
                                />
                                {field.saveValue === 'password' ? !showPassword ? <Visibility onClick={() => setShowPassword(true)} /> : <VisibilityOff onClick={() => setShowPassword(false)} /> : ""}
                                <div className="code-button">{field.saveValue === "email" && <button style={{ padding: "3px 5px 3px 5px", fontSize: "14px" }} disabled={0} onClick={sendCode}>{disabledCode ? "Wait..." : "Send Code"}</button >}</div>
                            </div>
                            )}
                            {validPassword === false ? <div style={{ color: "#661212", fontWeight: "400", fontSize: "16px" }} >{"Password should contain 8-20 characters, 1 uppercase, 1 lowercase, 1 special character, 1 number"}</div> :
                                validPhone === false ? <div style={{ color: "#661212", fontWeight: "400", fontSize: "16px" }} >{"Invalid Phone Number"}</div> : ""}

                            {errorMessage && <div style={{ color: "#661212", fontWeight: "400", fontSize: "16px" }} >{errorMessage}</div>}

                            {/* License Type dropdown*/}
                            <div>
                                <Dropdown
                                    label="License Type*"
                                    options={[
                                        { label: 'Standard', value: 'Standard' },
                                        { label: 'Premium', value: 'Premium' },
                                    ]}
                                    value={licenseType}
                                    onChange={handleLicenseTypeChange}
                                />
                            </div>

                            {/* SUbscription Type dropdown*/}
                            <div>
                                <Dropdown
                                    label="Subscription Type*"
                                    options={[
                                        { label: 'One Month', value: 'oneMonth' },
                                        { label: 'Six Months', value: 'sixMonth' },
                                        { label: 'One Year', value: 'oneYear' },
                                    ]}
                                    value={subType}
                                    onChange={handleSubTypeChange}
                                />
                            </div>

                            <div className="field button">
                                <button className="next" disabled={disabled} onClick={onRegisterClick}>Register</button>
                            </div>


                        </div>
                    </form>
                </div>
            </div>
        </div >
    )
}

export default RegisterPage;