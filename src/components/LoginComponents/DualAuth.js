import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { useEffect } from 'react';
import { Card } from "react-bootstrap";
import logo from "./vra_logo.png";
import { CardDescription, CardHeader } from "semantic-ui-react";
import Button from "react-bootstrap/Button";
import { useNavigate } from 'react-router-dom';
import './DualAuth.css'
import { sendVerificationEmailNotification } from "../emailNotification.js";

const DualAuthentication = () => {
  const user = JSON.parse(localStorage.getItem("user_unsigned"));

  const [isLoading, setLoading] = useState(false);
  const [isSending, setsending] = useState(false);
  const [code, setCode] = useState("");
  const [pass, setPass] = useState("");
  const navigate = useNavigate();

  useEffect(() => {
    if (isLoading) {
      simulateNetworkRequest().then(() => {
        setLoading(false);
      });
    }
  }, [isLoading]);

  useEffect(() => {
    if (isSending) {
      simulateNetworkRequest().then(() => {
        setsending(false);
      });
    }
  }, [isSending]);

  function simulateNetworkRequest() {
    return new Promise((resolve) => setTimeout(resolve, 2000));
  }

  //Randome code Generating fuction 
  const getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min);
  }

  //sending a text message to the email using the emailjs
  const handleClicksendEmail = (e) => {
    e.preventDefault();
    setsending(true);
    const code = getRandomInt(100000, 999999);
    setCode(code.toString());
    const templateParams = {
      from_name: "VRA Client Verification",
      to_name: user.firstName,
      subject: "VRA Client Verification",
      user_name: "VRA",
      message1: `Your Email Verification Code is ${code}. Please enter this code on the VRA Registration Page, to continue.`,
      cc_candidates: `${user.email},raviteja@virtualreviewassist.com`
    };
    sendVerificationEmailNotification(templateParams, () => { });
  }

  function handleSubmit(event) {
    event && event.preventDefault();
    if (pass !== null && pass !== "") {
      if (pass === code) {
        localStorage.setItem("dualAuth", true);
        localStorage.setItem("user", JSON.stringify(user));
        navigate("/");
      } else {
        window.alert("Invalid Passcode Authenticate again");
        localStorage.clear();
        navigate("/login");
      }
    } else {
      window.alert("Invalid Passcode Authenticate again");
      localStorage.clear();
      navigate("/login");
    }
  }

  return (

    <div className="container-Authe" >
      <Card className="text-content" >
        <CardHeader className="Header"  >VRA</CardHeader>
        <CardDescription className="Title">Two Factor Authentication</CardDescription>
        <br></br>
        <div className="form">
          <center><img src={logo} alt="Not available" className="logo" style={{ width: 60 }} /></center>
          <form>
            <div className="input-container" style={{ marginTop: 20 }}>
              <label>Email: </label>
              <label>{user.email}</label>
              <br></br>
              <br></br>
              <Button variant="primary" style={{ marginLeft: 0 }} disabled={isSending} onClick={!isSending ? handleClicksendEmail : null}> {isSending ? 'Sending…' : 'Send code'}</Button>
            </div>
            <br />
            <br />
            <div className="button-container" style={{ marginTop: 20 }}>
              <label>Enter OTP Sent: </label>
              <br />
              <input type="text" name="pass" onKeyDown={(e) => {
                if (e.key == "Enter" || e.key == "NumpadEnter" || e.keyCode === 13) {
                  e.preventDefault();
                  handleSubmit();
                }
              }}
                onChange={(e) => {
                  e.preventDefault();
                  setPass(e.target.value)
                }} />
              <br />
              <br />
              <Button onClick={handleSubmit}>Verify</Button>
            </div>
          </form>
        </div>
      </Card>
    </div>

  );
};
export default DualAuthentication;