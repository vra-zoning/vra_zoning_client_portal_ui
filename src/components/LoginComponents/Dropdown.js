
const Dropdown = (props) => {

    return (
    <div>
      <label style={{width:"100%", height:"30px", margin:"0 0 3% 3%", display:"flex", flexDirection:"row", position:"relative", fontWeight:"500"}}>
        {props.label}
        <select value={props.value} onChange={props.onChange} style={{borderRadius:"5px", marginLeft:"40%", position:"absolute", height:"100%", width:"50%"}}>
          {props.options.map((option) => (
            <option value={option.value}>{option.label}</option>
          ))}
        </select>
      </label>
      
      </div>
    );
  };

  export default Dropdown;