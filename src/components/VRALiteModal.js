import { useState } from "react";
import { Modal, CloseButton, Row, Button, Form, Col } from "react-bootstrap";
import "./VRALiteModal.css";
import axios from 'axios';

const VRALiteModal = (props) => {

    const API_Endpoint = "https://vrazoningclientws.azurewebsites.net"
    const [validated, setValidated] = useState(false);

    const [projectName, setProjectName] = useState("")
    const [city, setCity] = useState("")
    const [county, setCounty] = useState("")
    const [state, setState] = useState("")
    const [zip, setZip] = useState("")
    const [pcn, setPCN] = useState("");


    const handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        else {
            event.preventDefault();
            event.stopPropagation();
            props.handleClose();
            generateVraLite();
        }
        setValidated(true);
    };

    const generateVraLite = () => {
        getResources(county, pcn);
        
    }

    const getResources = async (county, pcn) => {
        const vraLiteResources = await axios.get(`${API_Endpoint}/api/VraLiteAPI/GetResources?pcnno=${pcn}&county=${county}`,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
            });
            window.open("./VRALiteReportPrint", "_blank")
        // console.log(vraLiteResources)
    }

    return (

        <div>
            <Modal
                show={props.vraLiteModal}
                onHide={props.handleClose}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header className='vra-lite-modal-header'>
                    <Modal.Title className="vra-lite-modal-title">VRA Lite Report Generation</Modal.Title>
                    <CloseButton variant="black" onClick={(e) => props.handleClose(e)} />
                </Modal.Header>

                <Modal.Body>
                    <Form noValidate validated={validated} onSubmit={handleSubmit}>


                        <Row className="mb-3">
                            <Form.Group as={Col} md="12" controlId="validationCustom01">
                                <Form.Label className='zoning-form-label'>Project Name</Form.Label>
                                <Form.Control
                                    required
                                    type="text"
                                    placeholder="Project Name"
                                    value={projectName}
                                    onChange={(e) => setProjectName(e.target.value)}
                                />
                                <Form.Control.Feedback type='invalid'>Please Name your project</Form.Control.Feedback>
                            </Form.Group>
                        </Row>

                        <Row className="mb-3">
                            <Form.Group as={Col} md="8" controlId="validationCustom02">
                                <><Form.Label className='zoning-form-label'>PCN</Form.Label>
                                    <Form.Control
                                        required
                                        type="text"
                                        placeholder="eg: A-10-30-18-3ZM-000006-00021.0"
                                        value={pcn}
                                        onChange={(e) => setPCN(e.target.value)}
                                    /></>
                                <Form.Control.Feedback type='invalid'>Please enter Street address</Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group as={Col} md="4" controlId="validationCustom03">
                                <Form.Label className="ssummary-form-label">City</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="City"
                                    value={city}
                                    onChange={(e) => setCity(e.target.value)}
                                    required
                                />
                                <Form.Control.Feedback type="invalid">
                                    Please enter a City.
                                </Form.Control.Feedback>
                            </Form.Group>
                        </Row>

                        <Row className="mb-3">
                            <Form.Group as={Col} md="6" controlId="validationCustom04">
                                <Form.Label className="ssummary-form-label">County</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="County"
                                    value={county}
                                    onChange={(e) => setCounty(e.target.value)}
                                    required
                                />
                                <Form.Control.Feedback type="invalid">
                                    Please provide a County.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group as={Col} md="3" controlId="validationCustom05">
                                <Form.Label className="ssummary-form-label">State</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="State"
                                    value={state}
                                    onChange={(e) => setState(e.target.value)}
                                    required
                                />
                                <Form.Control.Feedback type="invalid">
                                    Please provide a valid state.
                                </Form.Control.Feedback>
                            </Form.Group>
                            <Form.Group as={Col} md="3" controlId="validationCustom06">
                                <Form.Label className="ssummary-form-label">Zip</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Zip"
                                    value={zip}
                                    onChange={(e) => setZip(e.target.value)}
                                    required
                                />
                                <Form.Control.Feedback type="invalid">
                                    Please provide a valid zip.
                                </Form.Control.Feedback>
                            </Form.Group>
                        </Row>

                        <Button className="generate-button" type="submit">Generate Report</Button>
                    </Form>
                </Modal.Body>

            </Modal>
        </div>
    );

}

export default VRALiteModal;