import "./VRALiteReportPrint.css";
import React, { useRef } from 'react';
import { useReactToPrint } from 'react-to-print';

import VRALiteReport from "./VRALiteReport";
import {Button} from "react-bootstrap";

const VRALiteReportPrint = () => {

    const componentRef = useRef();
    const handlePrint = useReactToPrint({
        content: () => componentRef.current,
    });

    return(
        <div>
            {/* <div style={{display:"none"}}> */}
            <div>
            <VRALiteReport ref={componentRef} />
            </div>
            <div class="row h-100" className="print-button-container">
            <Button className="print-report" onClick={handlePrint}>PRINT VRA LITE REPORT</Button>
            </div>
        </div>
    );
}

export default VRALiteReportPrint;