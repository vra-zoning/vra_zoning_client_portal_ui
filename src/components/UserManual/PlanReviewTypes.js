import React from 'react';
// import Slider from 'infinite-react-carousel';
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader  
import slide1 from "./PlanReviewTypesImages/Slide1.PNG";
import slide2 from "./PlanReviewTypesImages/Slide2.PNG";
import slide3 from "./PlanReviewTypesImages/Slide3.PNG";
import slide4 from "./PlanReviewTypesImages/Slide4.PNG";
import slide5 from "./PlanReviewTypesImages/Slide5.PNG";
import slide6 from "./PlanReviewTypesImages/Slide6.PNG";
import slide7 from "./PlanReviewTypesImages/Slide7.PNG";
import slide8 from "./PlanReviewTypesImages/Slide8.PNG";
import slide9 from "./PlanReviewTypesImages/Slide9.PNG";
import slide10 from "./PlanReviewTypesImages/Slide10.PNG";
import slide11 from "./PlanReviewTypesImages/Slide11.PNG";
import slide12 from "./PlanReviewTypesImages/Slide12.PNG";
import slide13 from "./PlanReviewTypesImages/Slide13.PNG";
import slide14 from "./PlanReviewTypesImages/Slide14.PNG";
import slide15 from "./PlanReviewTypesImages/Slide15.PNG";


const PlanReviewTypes = () => {
    const images = [slide1, slide2, slide3, slide4, slide5, slide6, slide7, slide8, slide9, slide10, slide11, slide12,  slide13, slide14, slide15  ];

    return (
      <div className="col">
          <Carousel infiniteLoop showArrows={false} autoPlay stopOnHover dynamicHeight showThumbs={false} showStatus={false}>
            {images.map((item) => {
              return <div>
                        <img src={item} />
                    </div>
            })}
          </Carousel>
        </div>
    )
  }

export default PlanReviewTypes;