import React from 'react';
// import Slider from 'infinite-react-carousel';
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader  
import slide1 from "./AutoCADGuideLinesImages/Slide1.PNG";
import slide2 from "./AutoCADGuideLinesImages/Slide2.PNG";
import slide3 from "./AutoCADGuideLinesImages/Slide3.PNG";
import slide4 from "./AutoCADGuideLinesImages/Slide4.PNG";
import slide5 from "./AutoCADGuideLinesImages/Slide5.PNG";
import slide6 from "./AutoCADGuideLinesImages/Slide6.PNG";
import slide7 from "./AutoCADGuideLinesImages/Slide7.PNG";
import slide8 from "./AutoCADGuideLinesImages/Slide8.PNG";
import slide9 from "./AutoCADGuideLinesImages/Slide9.PNG";
import slide10 from "./AutoCADGuideLinesImages/Slide10.PNG";
import slide11 from "./AutoCADGuideLinesImages/Slide11.PNG";
import slide12 from "./AutoCADGuideLinesImages/Slide12.PNG";
import slide13 from "./AutoCADGuideLinesImages/Slide13.PNG";
import slide14 from "./AutoCADGuideLinesImages/Slide14.PNG";
import slide15 from "./AutoCADGuideLinesImages/Slide15.PNG";


const AutoCADGuideLines = () => {
    const images = [slide1, slide2, slide3, slide4, slide5, slide6, slide7, slide8, slide9, slide10, slide11, slide12,  slide13, slide14, slide15  ];

    return (
      <div className="col">
          <Carousel infiniteLoop showArrows={false} autoPlay stopOnHover dynamicHeight showThumbs={false} showStatus={false}>
            {images.map((item) => {
              return <div>
                        <img src={item} />
                    </div>
            })}
          </Carousel>
        </div>
    )
  }

export default AutoCADGuideLines;