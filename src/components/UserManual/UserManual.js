import React from "react";
import { Card, Button } from "react-bootstrap";
import "../Dashboard.css";
import "./UserManual.css";

const UserManual = (props) => {

  let videos = require("./videos.png");
  let books = require("./books1.png");
  let usermanual = require("./user-manual1.png");

  return (
    <div>
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1"
      />
      <title>Virtual Review Assist</title>
      <link rel="preconnect" href="https://fonts.googleapis.com" />
      <link
        rel="preconnect"
        href="https://fonts.gstatic.com"

      />
      <link
        href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;500;700;900&display=swap"
        rel="stylesheet"
      />
      <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
      />
      <link rel="stylesheet" href="/css/bootstrap.min.css" />
      <link rel="stylesheet" href="/css/style.css" type="text/css" />
      {/* Below Files are for the File manager - Documentation can be found at https://plugins.krajee.com/file-input */}
      <link
        href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.2.2/css/fileinput.min.css"
        media="all"
        rel="stylesheet"
        type="text/css"
      />

      <div className="row h-100" >
        <div className="col-md-1"></div>
        <div className="col-md-11" >

          <div style={{ height: "50px", borderRadius: "6px", backgroundColor: "rgb(1, 78, 106)", color: "white", display: "flex", alignItems: "center", justifyContent: "center" }}>
            <h2 style={{ margin: "0" }}>
              <center><b > User Manual </b></center>
            </h2>
          </div>
          <br></br><br></br><br></br>

          <div className="row h-400">
            <div className="col-md-3">
              <Card className="usermanual-cards">
                <Card.Img className='usermanual-cards-image' variant="top" src={videos} />
                <Card.Body className="usermanual-cards-body">
                  <Button className='usermanual-cards-button' variant="primary">Videos</Button>
                </Card.Body>
              </Card>
            </div>
            <div className="col-md-1"></div>

            <div className="col-md-3">
              <Card className="usermanual-cards">
                <Card.Img className='usermanual-cards-image' variant="top" src={usermanual} />
                <Card.Body className="usermanual-cards-body">
                  <Button onClick={() => props.setScreenName('manuals')} className='usermanual-cards-button' variant="primary">Manuals</Button>
                </Card.Body>
              </Card>
            </div>

            <div className="col-md-1"></div>

            <div className="col-md-3">
              <Card className="usermanual-cards">
                <Card.Img className='usermanual-cards-image' variant="top" src={books} />
                <Card.Body className="usermanual-cards-body">
                  <Button className='usermanual-cards-button' variant="primary">Books</Button>
                </Card.Body>
              </Card>
            </div>

          </div>

        </div>

      </div>
    </div>
  );
}

export default UserManual;
