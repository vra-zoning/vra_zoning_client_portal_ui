import React from "react";
import { useState, useEffect } from "react";
import PlanReviewTypes from "./PlanReviewTypes";
import AutoCADGuideLines from "./AutoCADGuidelines.js"

const Manuals = () => {

        return(
            <div>
                <div className="row manuals-title">
                    <h1><b>What is VRA?</b></h1>
                </div>

                <div className="manuals-body">
                    <div className='col-md-1'></div>
                    <PlanReviewTypes />
                    <div className='col-md-1'></div>
                </div>

                <br></br>
                <br></br>

                <div className="row manuals-title">
                    <h1><b>AutoCAD Developer Guidelines</b></h1>
                </div>

                <div className="manuals-body">
                    <div className='col-md-1'></div>
                    <AutoCADGuideLines />
                    <div className='col-md-1'></div>
                </div>

            </div>
        )

}

export default Manuals;