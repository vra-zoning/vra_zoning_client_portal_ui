import React from "react";
import { Card } from "react-bootstrap";
import OurProductsModal from "./OurProductsModal.js";
import "./Dashboard.css";

class NewProjectModal extends React.Component {

  fileList = ["a/inputs/f", "a/outputs/f"];
  hidelevel = true;
  constructor(props) {
    super(props);
    this.state = {
      file: null,
      modal: false,
      modalData: false,
      name: "",
      OurProductsModal: false,
      fileData: null,
      flag: false,
      isCheckListDataReceived: false,
      checklistData: "",
    };
  }

  siteSummaryUpload = (projectName, county, siteName, projectId, bufferList, reviewType) => {
    this.props.siteSummaryUpload(projectName, county, siteName, projectId, bufferList, reviewType)
    window.alert("Site Summary Report Generation is in Progress");
  }

  openOurProductsModal() {
    this.setState({ OurProductsModal: true });
  }

  closeOurProductsModal() {
    this.setState({ OurProductsModal: false });
  }

  //temp check
  siteSummaryUpload = (projectName, county, siteName, projectId, bufferList, reviewType) => {
    this.props.siteSummaryUpload(projectName, county, siteName, projectId, bufferList, reviewType)
    //this.modalClose();
    // window.alert("Site Summary Report Generation is in Progress");
  }


  render() {

    let folderIcon = require("./images/folder-icon.png")

    return (

      <div className="col-md-3 new-project-upload-card-container">
        <Card className="new-project-upload-card" onClick={(e) => this.openOurProductsModal(e)}>
          <Card.Body className="new-project-upload-card-body">
            <Card.Img className="new-project-card-image" src={folderIcon} />
            <u>Start Project Here</u>
          </Card.Body>
        </Card>

        {this.state.OurProductsModal && (
          <OurProductsModal
            OurProductsModal={this.state.OurProductsModal}
            handleClose={(e) => this.closeOurProductsModal(e)}
            setRefresh={this.props.setRefresh}
            refresh={this.props.refresh}/>
        )}
      </div>
    );
  }
}

export default NewProjectModal;
