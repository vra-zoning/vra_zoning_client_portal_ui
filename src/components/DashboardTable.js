
import { React, useState, useEffect } from "react";
import "./Dashboard.css"

const DashboardTable = (props) => {

    const [searchVal, setSearchVal] = useState("");
    const [sortType, setSortType] = useState('timestampr');
    const [files, setFiles] = useState([]);
    const [records, setRecords] = useState([])


    useEffect(() => {
        //sort the azure files
        const newArray = [...props.files.sort((item1, item2) => {
            const county1 = item1.file_path && item1.file_path.split("/")[3].toLowerCase();
            const county2 = item2.file_path && item2.file_path.split("/")[3].toLowerCase();
            const projName1 = item1.file_path && item1.file_path.split("/")[2].toLowerCase();
            const projName2 = item2.file_path && item2.file_path.split("/")[2].toLowerCase();

            if (sortType === "county") {
                return county1 > county2 ? 1 : -1;
            }
            else if (sortType === "projectName") {
                return projName1 > projName2 ? 1 : -1;
            }
            return 1;
        })];
        setFiles(newArray);

        if (props.project_records.data && (sortType === "timestamp" || sortType === "timestampr")) {
            // //sort the database records for timestamp
            const newArray1 = [...props.project_records.data.sort((item1, item2) => {
                const timestamp1 = item1.time_stamp
                const timestamp2 = item2.time_stamp

                if (sortType === "timestamp") {
                    // console.log("sort 1", timestamp1, timestamp2)
                    return timestamp1 > timestamp2 ? 1 : -1;
                }
                else if (sortType === "timestampr") {
                    // console.log("sort 2", timestamp1, timestamp2)
                    return timestamp2 > timestamp1 ? 1 : -1;
                }
                return 1;
            })];
            var finalFilesArray = [];
            for (var i = 0; i < newArray1.length; i++) {
                var filepath = newArray1[i].file_path
                for (var j = 0; j < props.files.length; j++) {
                    var file = props.files[j];
                    if (file && file.file_path === filepath) {
                        finalFilesArray.push(file);
                    }
                }
            }
            if (finalFilesArray) {
                setFiles(finalFilesArray);
            }
        }
        setRecords(props.project_records.data)
        // }
    }, [sortType, props.files, props.project_records.data]);

    return (

        <div>
            {/* Search with keyword textbox */}
            <div className="row">
                <div className="col-md-6"></div>
                <div className="col-md-3">
                    <input id="projectName" name="pname" type="text" className="text-field" placeholder="Search" style={{ height: "35px" }} onChange={(e) => setSearchVal(e.target.value)}></input>
                </div>
                {/* END Search with keyword textbox*/}

                {/* sort dropdown */}

                <div className="col-md-3">
                    <label >
                        <select onChange={(e) => setSortType(e.target.value)} style={{ height: "35px", borderColor: "#c7c7c7", borderRadius: "5px", color: "#7f7a80", textAlign: "center" }}>
                            <option value="projectName">Sort by Project Name A to Z</option>
                            <option value="county">Group by County</option>
                            <option value="timestamp">Oldest First</option>
                            <option value="timestampr">Most Recent First</option>
                        </select>
                    </label>
                </div>

                {/* END sort dropdown */}

            </div>

            <br />
            <table
                cellPadding={0}
                cellSpacing={0}
                border={0}
                className="datatable table table-bordered1"
            >

                <thead className="thead-dark" style={{ borderTopLeftRadius: "12px", borderTopRightRadius: "12px" }}>
                    <tr>
                        <th className="text-center" style={{ borderTopLeftRadius: "12px" }}>Project Name</th>
                        <th className="text-center" >Site Address/ PCN</th>
                        <th className="text-center" >County Name</th>
                        <th className="text-center">Review Type</th>
                        <th className="text-center">Document Name</th>
                        <th className="text-center" style={{ width: "150px", borderTopRightRadius: props.status[0] === "inprogress" ? "12px" : "0px" }}>
                            Status
                        </th>
                        {(props.status[0] !== "inprogress") && <th className="text-center" style={{ width: "200px", borderTopRightRadius: "12px" }}>
                            Reports
                        </th>}
                    </tr>
                </thead>

                {/** We Insert the Report Data Dynamically Here {REPORT_DATA} */}

                <tbody>
                    {files
                        .filter((record) => !searchVal.length || record.file_path
                            .toString()
                            .toLowerCase()
                            .includes(searchVal.toString().toLowerCase())
                        )
                        .map((row, i) =>
                        ((props.status.includes(records.filter(obj => obj.file_path === row.file_path)[0] && records.filter(obj => obj.file_path === row.file_path)[0].status.replace(/\s+/g, '').toLowerCase())) &&
                            <tr key={`dash-table-row-${i}`}>
                                <td align="center">{(row.file_path !== null) ? row.file_path.split("/")[2] : " "}</td>
                                <td align="center" >{records && records.filter(obj => obj.file_path === row.file_path)[0] &&
                                    (records.filter(obj => obj.file_path === row.file_path)[0].site_address ?
                                        records.filter(obj => obj.file_path === row.file_path)[0].site_address :
                                        records.filter(obj => obj.file_path === row.file_path)[0].pcn)}</td>
                                <td align="center">{(row.file_path !== null) ? row.file_path.split("/")[3] : " "}</td>
                                <td>
                                    {records && records.filter(obj => obj.file_path === row.file_path)[0] &&
                                        records.filter(obj => obj.file_path === row.file_path)[0].review_type}
                                </td>
                                <td align="center">{(row.file_path !== null) ? row.file_path.split("/").pop() : " "}</td>
                                <td align="center">{
                                    records && records.filter(obj => obj.file_path === row.file_path)[0] &&
                                    records.filter(obj => obj.file_path === row.file_path)[0].status
                                }
                                </td>

                                {(props.status[0] !== "inprogress") && <td align="center">
                                    {
                                        row.Reports.map((reportrow, i) => (
                                            <div align="center" key={`link-${i}`}>

                                                <a href="# " onClick={() => props.func(reportrow.link)}>
                                                    {reportrow.link.split("/").pop()}
                                                </a>

                                            </div>
                                        )
                                        )
                                    }
                                </td>}
                            </tr>
                        ))}
                </tbody>

            </table>
        </div>
    );

}

export default DashboardTable;