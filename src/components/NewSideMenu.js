import React from "react";
import Modal from "./Modal.js";
import { Button } from "react-bootstrap";
import "./NewSideMenu.css";

class NewSideMenu extends React.Component {
  fileList = ["a/inputs/f", "a/outputs/f"];

  hidelevel = true;
  constructor(props) {
    super(props);
    this.state = {
      file: null,
      modal: false,
      modalData: false,
      name: "",
      fileData: null,
      modalInputName: "",
      flag: false,
      isCheckListDataReceived: false,
      checklistData: "",

    };
  }
  modalOpen() {
    this.setState({ modal: true });
  }
  modalClose() {
    this.setState({
      modalInputName: "",
      modal: false,
    });
    window.location.reload(false);
  }
  handleSubmit(e) {
    this.setState({ name: this.state.modalInputName });
    this.modalClose();
  }
  handleChangeSubmit(e) {
    this.fileCheckUpload(this.state.file);
    //this.setState({ name: this.state.modalInputName });

    this.setState({
      modalInputName: "ggg",
      modalData: true,
    });
    this.modalClose();
  }

  render() {
    let { files } = this.fileList

    return (
      <div style={{ padding: "0px 0px 0px 0px" }}>
        <br />
        <br />
        <ul className="nav">
          <li>
            <Button onClick={() => this.props.setScreenName("landuse")} >
              <i className="fas fa-building" /*style={{ marginRight: "5px" }}*/ />
              Project Landuse Review
            </Button>
          </li>

          <li style={{ marginTop: "5px" }}>

            <Button onClick={() => this.props.setScreenName("landscape")} >
              <i className="fas fa-image" /*style={{ marginRight: "5px" }}*/></i>
              Landscape Review
            </Button>
          </li>
          <li style={{ marginTop: "5px" }}>

            <Button onClick={() => this.props.setScreenName("transport")} >
              <i className="fas fa-truck" /*style={{ marginRight: "4px" }}*/ />
              Transportation Review
            </Button>
          </li>
          <li style={{ marginTop: "5px" }}>
            <Button onClick={() => this.props.setScreenName("sitesumm")} >
              <i className="fas fa-list-alt" /*style={{ marginRight: "5px" }}*/ />
              Site Summary
            </Button>
          </li>

          {/* <li style={{ marginTop: "5px" }}>
            <Button onClick={() => this.props.setScreenName("platting")} style={{ textAlign: "left", width: "212px" }}>
              <i className="fas fa-info-circle" style={{ marginRight: "5px" }} />
              Platting Management
            </Button>
          </li> */}

          <li style={{ marginTop: "5px" }}>

            <Button onClick={() => this.props.setScreenName("usermanual")}>
              <i className="fas fa-info-circle" /*style={{ marginRight: "5px" }}*/ />
              User Manual
            </Button>
          </li>
        </ul>

        <Modal show={this.state.modal} handleClose={(e) => this.modalClose(e)}>

          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header thead-dark">
                <h4 className="modal-title" >New model file upload</h4>
                <button type="button" className="btn-primary" onClick={(e) => this.modalClose(e)} data-dismiss="modal" aria-label="Close">
                  X
                </button>
              </div>
              <div className="modal-body">
                <br></br>
                {/* <FileUpload ></FileUpload> */}
              </div>
            </div>
          </div>
        </Modal>

        <div className="modal" tabIndex="-1" role="dialog">

        </div>


      </div>
    );
  }
}

export default NewSideMenu;
