// THIS IS SAMPLE CODE ONLY - NOT MEANT FOR PRODUCTION USE
import { BlobServiceClient } from "@azure/storage-blob";

const sasToken =
  "?sv=2020-08-04&ss=bfqt&srt=sco&sp=rwdlacupitfx&se=2023-01-02T04:57:59Z&st=2022-04-14T19:57:59Z&spr=https&sig=oq8h%2B69B2xTG4pebrgchHCYEWx1Wi7a4Bl%2FVgeWJJn0%3D"; // Fill string with your SAS token
const containerName = `testcontainer`;
const storageAccountName = "vrazoning"; // Fill string with your Storage resource name

//will store the path where the file is stored in Azure
var filePath = ""

// Feature flag - disable storage feature to app if not configured
export const isStorageConfigured = () => {
  return !(!storageAccountName || !sasToken);
};

// // return list of blobs in container to display
// const getBlobsInContainer = async (containerClient) => {
//   const returnedBlobUrls = [];

//   // get list of blobs in containerdepcheck
//   // // eslint-disable-next-line
//   // for await (const blob of containerClient.listBlobsFlat()) {
//   //   // if image is public, just construct URL
//   //   returnedBlobUrls.push(
//   //     `https://${storageAccountName}.blob.core.windows.net/${containerName}/${blob.name}`
//   //   );
//   // }

//   return returnedBlobUrls;
// };

const createBlobInContainer = async (containerClient, file, username, projectName, siteName, county, path) => {
  // create blobClient for container
  // console.log(projectName)
  // console.log(siteName)
  // console.log(county)
  var curr_year = new Date().getFullYear();
  var path1 = path || username + "/" + curr_year + "/" + projectName + "/" + county + "/inputs/" + file.name;
  // var path = username+"/"+"model1.dwg"+"/inputs/" + file.name;
  const blobClient = containerClient.getBlockBlobClient(path1);

  // set mimetype as determined from browser with file upload control
  const options = { blobHTTPHeaders: { blobContentType: file.type } };

  // upload file
  await blobClient.uploadBrowserData(file, options);
  await blobClient.setMetadata({ UserName: "vamsibvv" });

  //updatig the file_path to export the variable to the database
  filePath = path1;
};

const uploadFileToBlob = async (file, username, projectName, siteName, county, path) => {
  if (!file) return [];

  // get BlobService = notice `?` is pulled out of sasToken - if created in Azure portal
  const blobService = new BlobServiceClient(
    `https://${storageAccountName}.blob.core.windows.net/?${sasToken}`
  );
  // get Container - full public read access
  const containerClient = blobService.getContainerClient(containerName);

  // upload file
  await createBlobInContainer(containerClient, file, username, projectName, siteName, county, path);

  // get list of blobs in container
  return filePath;
};
// </snippet_uploadFileToBlob>

export default uploadFileToBlob;
