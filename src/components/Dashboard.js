import React from "react";
import { useState, useEffect } from "react";
import NewSideMenu from "./NewSideMenu.js";
import { Card, Button } from "react-bootstrap";
import NewProjectModal from "./NewProjectModal.js";
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Footer from "./Footer.js";
import SiteSummary from "./DashboardSideMenu/SiteSummary.js";
import LandUse from "./DashboardSideMenu/LandUse.js";
import SimpleSlider from './SimpleSlider.js';
import Transportation from "./DashboardSideMenu/Transportation.js";
import Landscape from "./DashboardSideMenu/Landscape.js";
import UserManual from "./UserManual/UserManual.js";
import DashboardTable from "./DashboardTable.js";
import Manuals from "./UserManual/Manuals.js";
import axios from "axios";
import PlattingManagement from "./PlattingManagement/PlattingManagement.js";
import { publicIpv4 } from 'public-ip';
import VRALiteModal from "./VRALiteModal.js";

var test = 10;
const Dashboard = () => {
  const [screenName, setScreenName] = useState("dashboard");

  const user = localStorage.getItem("user") && JSON.parse(localStorage.getItem("user"));
  const isAuthenticated = user && true;

  const API_Endpoint ="https://vrazoningclientws.azurewebsites.net"

  const [customerId] = [null]
  var [files, setFiles] = useState([]);
  const [refresh, setRefresh] = useState(0);
  const [vraLiteModal, setVRALiteModal] = useState(false)

  const formData = new FormData();
  formData.append("LastName", user.family_name);
  formData.append("FirstName", user.given_name);
  formData.append("EmailId", user.email);
  formData.append("username", user.nickname);
  const [projectRecords, setProjectRecords] = useState({});

  // var files=[{"file_path":"temp1/inputs/file1.dwg", "file_status":"Completed", "Reports":[{"link": "temp1/outputs/report1.pdf"}, {"link":"temp1/outputs/report2.pdf"}]},{"file_path":"temp1/inputs/file2.dwg", "file_status":"In Progress", "Reports":[]}]
  var url = "https://vrazoningazurews.azurewebsites.net";

  const getDashboardFiles = async () => {

    //fetch the project details from the VRA_ZoningProjects table
    const records = await axios.get(`${API_Endpoint}/api/report/${user.email}`,
      {
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      })
    setProjectRecords(records)

    //fetch files from azure blob
    fetch(url + "/api/File/GetDirs?email=" + user.email, {
      method: "GET",
    })
      .then((response) => response.json())
      .then((data) => {

        if (JSON.stringify(files) !== JSON.stringify(data))
          setFiles(data);

      });
  }

  useEffect(() => {
    getDashboardFiles();
  }, [refresh, setRefresh])

  let profileIcon = require("./ViewProfile/pro-pic-2.jpg");

  const downloadReport = (fileName) => {
    const formData = new FormData();
    formData.append("customerId", sessionStorage.getItem("customerId"));

    fetch(url + "/api/File/DownloadFile?fileName=" + fileName, {
      method: "POST",
    }).then(res => res.blob())
      .then(blob => {
        var csvURL = window.URL.createObjectURL(blob);
        var tempLink = document.createElement('a');
        var fileNameSplit = fileName.split("/").pop()
        tempLink.href = csvURL;
        tempLink.setAttribute('download', fileNameSplit);
        tempLink.click();
      });
  };
  sessionStorage.setItem("customerId", customerId);
  let ip = 0;
  publicIpv4().then((msg) => {
    ip = msg;
  });

  const [key, setKey] = useState('Completed');

  return (
    isAuthenticated && (
      <div>
        <div>
          <div>
            <meta
              name="viewport"
              content="width=device-width, initial-scale=1"
            />
            <title>Virtual Review Assist</title>
            <link rel="preconnect" href="https://fonts.googleapis.com" />
            <link
              rel="preconnect"
              href="https://fonts.gstatic.com"

            />
            <link
              href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;500;700;900&display=swap"
              rel="stylesheet"
            />
            <link
              rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
            />
            <link rel="stylesheet" href="/css/bootstrap.min.css" />
            <link rel="stylesheet" href="/css/style.css" type="text/css" />
            {/* Below Files are for the File manager - Documentation can be found at https://plugins.krajee.com/file-input */}
            <link
              href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.2.2/css/fileinput.min.css"
              media="all"
              rel="stylesheet"
              type="text/css"
            />
            {/* HEADER */}
            <div>
              {/* <Navbar /> */}
            </div>
            {/* END HEADER */}
            {/* USER HOME */}
            <div className="container h-100 py-5">
              <div className="row h-100 mx-0">
                {/* LEFT COLUMN */}
                <div className="col-md-2 nav-sidebar border-right pb-4">
                  <div className="row">
                    <Card className='userprofile-icon'>
                      <Card.Link href="./UserProfile">
                        <center><Card.Img variant="top" className="cardstyle"  src={profileIcon} /></center>
                        <Card.Body className="card-body">Welcome, <br></br>{user.firstName}<br/><br/>To your VRA Planning and Zoning Software Applicant Portal.</Card.Body>
                      </Card.Link>
                      <center><Card.Link href="./UserProfile" > View Profile</Card.Link></center>
                    </Card>
                    <NewSideMenu setScreenName={setScreenName}></NewSideMenu>
                  </div>
                </div>
                {/* END LEFT COLUMN */}
                {/* DASHBOARD + NEW PROJECT */}
                <div className="col-md-10">
                  {screenName === "landuse" && <LandUse />}
                  {screenName === "landscape" && <Landscape />}
                  {screenName === "transport" && <Transportation />}
                  {screenName === "sitesumm" && <SiteSummary />}
                  {screenName === "usermanual" && <UserManual setScreenName={setScreenName} />}
                  {screenName === "manuals" && <Manuals />}
                  {screenName === "platting" && <PlattingManagement />}
                  {screenName === "dashboard" && <div className="col" >
                    {/* NEW PROJECT + CAROUSEL */}
                    <div className="row ">
                    <NewProjectModal setRefresh={setRefresh} refresh={refresh}/>
                      <div className="col-md-3 free-trial-container" >
                        <div className="free-trial vra-lite">
                          <h5 className="trial-heading">Try VRA Lite !</h5>
                          {/* <br></br> */}
                          <p style={{fontSize:"12px"}}>VRA Lite is a free service that gives the client an idea of our Site Summary products which are paid.</p>
                          <Button className="vra-lite-button" onClick={() =>{setVRALiteModal(true)}} disabled>Get VRA Lite Report</Button>
                        </div>
                      </div>
                    
                      <div className="col-md-6">
                        <SimpleSlider />
                      </div>
                    </div>
                    {/* END(NEW PROJECT + CAROUSEL) */}
                    <br />
                    <hr />
                    <div style={{ backgroundColor: "#014E6A", color: "white", height: "50px", borderRadius: "8px", display: "flex", alignItems: "center", justifyContent: "center" }}>
                      <h1 style={{ margin: "0" }}><b>Dashboard</b></h1>
                    </div>
                    <br />
                    <div className="col" >
                      <Tabs
                        id="controlled-tab"
                        activeKey={key}
                        onSelect={(k) => setKey(k)}
                        className="mb-3 myClass"
                      >
                        <Tab id="completedTab" eventKey="Completed" title="Completed" style={{ borderBottom: "1px solid #dee2e6" }} >
                          <DashboardTable status={["completed"]} files={files} func={downloadReport} project_records={projectRecords} />
                        </Tab>
                        <Tab id="inprogressTab" eventKey="Inprogress" title="In-progress">
                          <DashboardTable status={["inprogress", "error"]} files={files} func={downloadReport} project_records={projectRecords} />
                        </Tab>
                      </Tabs>

                    </div>
                  </div>}
                </div>
                {/* END(DASHBOARD + NEW PROJECT) */}
              </div>
            </div>
            {/* END USERHOME */}
            {/* FOOTER */}
            <Footer />
            {/* END FOOTER */}
          </div>
        </div>
        {
      vraLiteModal && (
        <VRALiteModal 
          vraLiteModal={vraLiteModal} 
          handleClose={() => setVRALiteModal(false)}
        />
      )
    }
      </div>
    )
  );
};

export const ThemeContext = React.createContext({
  vrauser: test,
});

export default Dashboard;
