import React from "react";
import "./PlattingManagement.css";
import SimpleSlider from "../SimpleSlider";
import PlattingUpload from "./plattingUpload.png";
import {Card} from "react-bootstrap";

const PlattingManagement = () => {

  return(

        <div>
            <div className="platting-title">
                Platting Management
            </div>

            <br></br>
            <br></br>

             {/* NEW PROJECT + CAROUSEL */}
             <div className="row ">
                <div className="col-md-1"></div>
                {/* Platting Management Uploads */}
                <div className="col-md-4 ">
                <Card className="platting-upload-card">
                    <Card.Body className="platting-upload-card-body">
                    <center><img className="platting-card-image" src={PlattingUpload} />
                    <br></br>
                    <b>Upload Platting Documents</b>
                    </center>
                    </Card.Body>
                </Card>
                </div>
                <div className="col-md-1"></div>
                <div className="col-md-6">
                    <SimpleSlider />
                </div>
            </div>
            {/* END(NEW PROJECT + CAROUSEL) */}
        </div>
  );
};

export default PlattingManagement;