import React from "react";
import "../../App.css";
// import { useAuth0 } from "@auth0/auth0-react";
import Dashboard from "../Dashboard";
// import { useHistory } from 'react-router-dom';
import Footer from "../Footer.js";
import background from "../images/homebg.jpg";

function Home() {
  //const { user, isAuthenticated } = useAuth0();
  // const [click, setClick] = useState(false);
  // const { loginWithRedirect, isAuthenticated } = useAuth0(!click);
 
  // console.log( localStorage.getItem("user"));
  // console.log(localStorage.getItem("dualAuth"))
  // const isAuthenticated = localStorage.getItem("user")&&localStorage.getItem("dualAuth");
  const isAuthenticated = localStorage.getItem("user");
  // console.log("Isauthenticat at home"+isAuthenticated);
  
  // const history = useHistory();
  // console.log("home");
  
  return !isAuthenticated ? (
    <div>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>Virtual Review Assist</title>
      <link rel="preconnect" href="https://fonts.googleapis.com" />
      <link rel="preconnect" href="https://fonts.gstatic.com"  />
      <link
        href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;500;700;900&display=swap"
        rel="stylesheet"
      />
      <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
      />
      <link rel="stylesheet" href="/css/bootstrap.min.css" />
      <link rel="stylesheet" href="/css/style.css" type="text/css" />
      {/* HEADER */}
      {/* <div>
      <Navbar></Navbar>
      </div> */}
      {/* END HEADER */}
      {/* HERO */}
      
      <div className="home-hero" style={{backgroundImage:`url(${background})`, backgroundSize:"cover"}}>
        
        <div className="container d-flex h-100 col-xl-10 col-xxl-8 px-4 py-5">
          <div className="row align-items-center g-lg-5 py-5">
            <div className="col-lg-10 mt-0 text-center text-lg-start text-white">
              {/* HERO CONTENT */}
              {/* <h1 className="display-4 fw-bold lh-1 mb-3 col-lg-10">
                <span className="display-6 fw-bold"></span>
                <br />
                VRA excels in performing rapid, uniform, comprehensive, and
                consistent code reviews using state-of-the-art, patent-pendingdcxdvc
                technologies.
              </h1> */}
              <p className="col-lg-10 fs-2">
                VRA excels in performing rapid, uniform, comprehensive, and
                consistent code reviews using state-of-the-art, patent-pending
                technologies.
              </p>
              {/* END HERO CONTENT */}
            </div>
          </div>
        </div>
      </div>
      {/* END HERO */}
      {/* CONTENT */}
      <div className="container py-5">
        <div className="row align-items-center">
          <div className="col-12 text-center">
            <h2 className="primary-color">Our Partners</h2>
          </div>
        </div>
        <div className="row align-items-center d-flex justify-content-center">
          <div className="col-md-3 py-4">
            <a href="#">
              <img src="images/UF-Logo.png" />
            </a>
          </div>
          <div className="col-md-3 py-4">
            <a href="#">
              <img src="images/Gainsville-Logo.jpg" />
            </a>
          </div>
        </div>
      </div>
      {/* END CONTENT */}
      {/* FOOTER */}
     <Footer />
      {/* END FOOTER */}
    </div>
  ) : (
    <Dashboard />
  );
}

export default Home;
