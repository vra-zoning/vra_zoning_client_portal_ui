import React from 'react';
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader  
import vra from "./images/vra.png";
import vra1 from "./images/vra1.png";
import vra2 from "./images/vra2.png";
import vra3 from "./images/vra3.png";
import vra4 from "./images/vra4.png";
import vra5 from "./images/vra5.png";
import vra6 from "./images/vra6.png";
import vra7 from "./images/vra7.png";
import vra8 from "./images/vra8.png";
import vra9 from "./images/vra9.png";
import vra10 from "./images/vra10.png";
import vra11 from "./images/vra11.png";
import vra12 from "./images/vra12.png";
import vra13 from "./images/vra13.png";


class SimpleSlider extends React.Component {

  render() {
    // const images = [vra, vra1, vra2, vra3, vra4, vra5, vra6, vra7, vra8, vra9, vra10, vra11, vra12, vra13];
    const images = [{key:1,value:vra},{key:2,value:vra1},{key:3,value:vra2},{key:4,value:vra3},{key:5,value:vra4},{key:6,value:vra5},{key:7,value:vra6},{key:8,value:vra7},{key:9,value:vra8},{key:10,value:vra9},{key:11,value:vra10},{key:12,value:vra11},{key:13,value:vra12},{key:14,value:vra13}]

    return (
      <div className="col">
        <div className="col-md-1"></div>
        <div className="col-md-11">
          <Carousel infiniteLoop showArrows={false} autoPlay stopOnHover dynamicHeight showThumbs={false} showStatus={false}>
            {images.map(({key,value}) => {
              return <div  key={key}>
                <img src={value}/>
              </div>
            })}
          </Carousel>
        </div>
      </div>
    )
  }
};

export default SimpleSlider;