import React, { useState } from "react";
import {useEffect} from 'react';
import { Card } from "react-bootstrap";
import  background from "./VRACard2.png";
import logo from "../images/vra_logo.png";
import "./LicenseTypes.css"

const LicenseCard = (props) => {
    
    return (
        <>
            <div>
                <Card className="license-card" style={{width:380, height :230,borderRadius:30,backgroundColor:'grey',backgroundImage: `url(${background})`}} >
                    <div className="d-flex align-items-center bd-highlight mb-3 example-parent" style={{ height: '100px',marginLeft: 30 }}>
                        <div style={{fontFamily: 'Arial Hebrew',fontSize: '1.5rem',color:'white',marginTop:20}}>{props.license}</div>
                    </div>
                    <img src={logo}  width={80} style={{marginLeft:250,marginBottom:30}} />
                    <div style={{width:380,heigh:230,display:'flex'}}>
                        <div style={{width:160,heigh:230, fontFamily: 'Arial Hebrew',color:'white',marginLeft:20}}>{props.name}<br></br>Member Since: {props.memberSince}</div>
                        <div style={{width:90,heigh:230, fontFamily: 'Arial Hebrew',color:'white', display:"flex", float:"right"}}>CustomerID:<br/>{props.customerID}</div>
                        <div style={{width:110,heigh:230, fontFamily: 'Arial Hebrew',color:'white',marginBottom:20}}>Expiry:<br/>{"XX/XX/XXXX"}</div>
                    </div>
                </Card>
            </div>
        </>
    
      );
    
    };
    
    export default LicenseCard;