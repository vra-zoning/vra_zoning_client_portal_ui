import React, { useState } from "react";
import Footer from "../Footer.js";
import { Button } from "react-bootstrap";
import PasswordCheck from "./PasswordCheck.js";
import PasswordModal from "./PasswordModal.js";
import { Dropdown, DropdownButton } from "react-bootstrap";
import DataReports from "../DataReports/DataReports";
import DataReportsBg from "../DataReports/DataReportsBg16.jpg";
import AdminDataReports from "../AdminPages/AdminDataReports.js"
import LicenseTypes from "./LicenseTypes.js";
import { publicIpv4 } from 'public-ip';
import axios from "axios";
import "./UserProfile.css";

export default function UserProfile() {

  const API_Endpoint ="https://vrazoningclientws.azurewebsites.net"
  const user = localStorage.getItem("user") && JSON.parse(localStorage.getItem("user"));
  const adminView = user && user.isAdmin;
  const [isEdit, setIsEdit] = useState(false);
  const [settingsScreen, setSettingsScreen] = useState("userProfile")
  const [userState, setUserState] = useState({
    firstName: user.firstName,
    lastName: user.lastName,
    email: user.email,
    mobile: user.mobile,
    mailingAddress: user.mailingAddress
  })

  let profilePic = require("./pro-pic-2.jpg")

  const changeValue = (e, field) => {
    const values = { ...userState };
    values[field] = e.target.value;
    setUserState(values);
  }

  publicIpv4().then((msg) => {
  });

  var [showModal, setShowModal] = useState(false);

  const editProfile = () => {

    if (isEdit) {
      const body = { ...userState };
      axios.put(`${API_Endpoint}/api/Customers/${user.email}`, body,
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
        })
        .then(function (response) {
          if (response && response.data) {
            userState.login = Date.now();
            localStorage.setItem("user", JSON.stringify(userState));
            setIsEdit(!isEdit);
          }
        })
        .catch(function (error) {
          window.alert("Internal server error updating user info")
          setIsEdit(!isEdit);
        });
    } else {
      setShowModal(true);
    }
  }

  var [pswdModalOpen, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <div>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>Virtual Review Assist</title>
      <link rel="preconnect" href="https://fonts.googleapis.com" />
      <link rel="preconnect" href="https://fonts.gstatic.com" />
      <link
        href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;500;700;900&display=swap"
        rel="stylesheet"
      />
      <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
      />
      <link rel="stylesheet" href="/css/bootstrap.min.css" />
      <link rel="stylesheet" href="/css/style.css" type="text/css" />
      {/* Below Files are for the File manager - Documentation can be found at https://plugins.krajee.com/file-input */}
      <link
        href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.2.2/css/fileinput.min.css"
        media="all"
        rel="stylesheet"
        type="text/css"
      />
      {/* HEADER */}
      <div>
        {/* <Navbar /> */}
      </div>
      {/* END HEADER */}
      {/* DASHBOARD */}
      <div style={settingsScreen === "DataReports" ? { backgroundImage: `url(${DataReportsBg})` } : {}}>
        <div className="container h-100 py-5" >
          <div className="row h-100 mx-0" >
            <div className="col-md-3">
              <div className="rowtop">
              </div>
              <div className="col">
                <ul className="nav">
                  <li className="user-prof-list">
                    <button onClick={() => { setSettingsScreen("userProfile") }} className="left-menu">
                      <i className="fas fa-user"></i>User Profile
                    </button>
                    <br /><hr className="user-hr"></hr>
                  </li>
                  <li className="user-prof-list">
                    <button onClick={() => { setSettingsScreen("DataReports") }} className="left-menu">
                      <i className="fas fa-home"></i>Data Reports
                    </button>
                    <br /><hr className="user-hr"></hr>
                  </li>
                  {adminView && <li className="user-prof-list">
                    <button onClick={() => { setSettingsScreen("AdminStats") }} className="left-menu">
                      <i className="fas fa-eye"></i>
                      Admin Data Reports
                    </button>
                    <br /><hr className="user-hr"></hr>
                  </li>}
                  <li className="user-prof-list">
                    <button onClick={() => { setSettingsScreen("LicenseTypes") }} className="left-menu">
                      <i className="fa fa-id-card" aria-hidden="true"></i>
                      License Types
                    </button>
                    <br /><hr className="user-hr"></hr>
                  </li>
                  <li className="user-prof-list">
                    <button className="left-menu">
                      <i className="fas fa-user-plus"></i>
                      Referrals
                    </button>
                    <br /><hr className="user-hr"></hr>
                  </li>
                  <li className="user-prof-list">
                    <button className="left-menu">
                      <i className="fas fa-credit-card" />
                      Transactions
                    </button>
                    <br />
                    <hr className="user-hr"></hr>
                  </li>
                  <li className="user-prof-list">
                    <button className="left-menu">
                      <i className="fas fa-link"></i>
                      URL generator
                    </button>
                    <br />
                    <hr className="user-hr"></hr>
                  </li>
                  <li className="user-prof-list">
                    <button className="left-menu">
                      <i className="fas fa-cogs"></i>
                      Settings
                    </button>
                    <br />
                    <hr className="user-hr"></hr>
                  </li>
                  <li className="user-prof-list">
                    <button className="left-menu">
                      <i className="fas fa-sign-out-alt"></i>
                      Logout
                    </button>
                    <br />
                    <hr className="user-hr"></hr>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-md-9 card-padding">
              {settingsScreen === "userProfile" &&
                <div>
                  <div style={{ height: "50px", borderRadius: "6px", backgroundColor: "rgb(1, 78, 106)", color: "white", display: "flex", alignItems: "center" }}>
                    <h2 style={{ margin: "0" }}>
                      <b > Settings </b>
                    </h2>
                  </div>
                  <br></br>
                  <br></br>
                  {/* PROFILE PICTURE */}
                  <div className="row h-5">
                    <div className="col-md-4"></div>
                    <div className="col-md-4">
                      <center>
                        <img variant="top" alt="Not available" style={{ width: '11rem', height: '11rem', borderRadius: "100%", objectFit: "cover" }} src={profilePic} />
                        <br></br>
                        <br></br>
                        <h3><b>{`${user.firstName}`}</b></h3>
                        <br></br>
                      </center>
                    </div>
                    <div className="col-md-4"></div>
                  </div>
                  {/* END PROFILE PICTURE */}
                  {/* START TEXT FIELDS */}
                  <div className="row">
                    <form>
                      <div className="row">
                        <div className="col-md-6">
                          <label style={{ color: "grey" }} htmlFor="fname">First Name</label><br />
                          {isEdit ? <input id="fname" name="fname" type='text' className='text-field' value={userState.firstName} onChange={(e) => changeValue(e, "firstName")} /> : <input id="fname" name="fname" type='text' className='text-field' value={userState.firstName} readOnly />}
                        </div>
                        <div className="col-md-6">
                          <label style={{ color: "grey" }} htmlFor="fname">Last Name</label><br />
                          {isEdit ? <input id="lname" name="lname" type='text' value={userState.lastName} className='text-field' onChange={(e) => changeValue(e, "lastName")} /> : <input id="lname" name="lname" type='text' className='text-field' value={userState.lastName} readOnly />}
                        </div>
                      </div>
                      <br />
                      <div className="row">
                        <div className="col-md-12">
                          <label style={{ color: "grey" }} htmlFor="address">Mailing Address</label><br />
                          {isEdit ? <input id="address" name="address" type='text' className='text-field' value={userState.mailingAddess} onChange={(e) => changeValue(e, "mailingAddress")} /> : <input id="address" name="address" type='text' className='text-field' value={userState.mailingAddress} readOnly />}
                        </div>
                      </div>
                      <br />
                      <div className="row">
                        <div className="col-md-6">
                          <label style={{ color: "grey" }} htmlFor="email">Contact Email</label><br />
                          <input id="email" name="email" type='text' className='text-field' value={userState.email} readOnly />
                        </div>
                        <div className="col-md-6">
                          <label style={{ color: "grey" }} htmlFor="mobile">Phone Number</label><br />
                          {isEdit ? <input id="mobile" name="mobile" type='text' className='text-field' value={userState.mobile} onChange={(e) => changeValue(e, "mobile")} /> : <input id="mobile" name="mobile" type='text' className='text-field' value={userState.mobile} readOnly />}
                        </div>
                      </div>
                      <br />
                      <div className="row">
                        <div className="col-md-6">
                          <label style={{ color: "grey" }} htmlFor="company">Company</label><br />
                          {isEdit ? <input id="company" name="company" type='text' className='text-field' value={userState.company} onChange={(e) => changeValue(e, "company")} /> : <input id="company" name="company" type='text' className='text-field' value={userState.company} readOnly />}
                        </div>
                        <div className="col-md-6">
                          <label style={{ color: "grey" }} htmlFor="work">Work/Affiliations</label><br />
                          {isEdit ? <input id="work" name="work" type='text' className='text-field' value={userState.work} onChange={(e) => changeValue(e, "work")} /> : <input id="work" name="work" type='text' className='text-field' value={userState.work} readOnly />}
                        </div>
                      </div>
                      <br></br>
                      <div className="row">
                        <div className="col-md-6">
                          <label style={{ color: "grey" }} htmlFor="payment">Payment Method</label><br />
                          <DropdownButton id="dropdown-basic-button" title="Payment Method">
                            <Dropdown.Item href="#/action-1">Check</Dropdown.Item>
                            <Dropdown.Item href="#/action-2">Credit Card</Dropdown.Item>
                            <Dropdown.Item href="#/action-3">Apple Pay</Dropdown.Item>
                          </DropdownButton>
                        </div>
                      </div>
                      <br></br>
                    </form>
                    <div className="row">
                      <button onClick={handleShow} style={{ color: "#014E6A", cursor: "pointer", width: "max-content" }}>
                        change password
                      </button>
                      <PasswordModal
                        pswdModalOpen={pswdModalOpen}
                        handleClose={handleClose}
                      />
                    </div>
                    <div >
                      <Button type="button" style={{ color: "white", backgroundColor: "#014E6A", borderColor: "#014E6A", fontSize: "14px", float: "right" }} onClick={() => { editProfile() }}>
                        <i className="fas fa-user-edit" /> {isEdit ? "Update Changes" : "Edit Profile"}
                      </Button>
                      {showModal && <PasswordCheck authEditProfile={setIsEdit} pswdCheckModal={showModal} handleCheckClose={() => setShowModal(false)}></PasswordCheck>}
                    </div>
                  </div>
                  {/* END TABLE */}
                </div>}
              {settingsScreen === "DataReports" && <DataReports />}
              {settingsScreen === "AdminStats" && <AdminDataReports />}
              {settingsScreen === "LicenseTypes" && <LicenseTypes />}
            </div>
          </div>
        </div>
      </div>
      <Footer></Footer>
    </div>
  );
}
