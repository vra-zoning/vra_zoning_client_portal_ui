import {React, useState} from 'react';
import { Modal, Button } from "react-bootstrap";
import axios from 'axios';


    const PasswordCheck = (props) => {

      const API_Endpoint ="https://vrazoningclientws.azurewebsites.net"

      const [success, setSuccess] = useState(false);
      const [pass, setPass] = useState("");
      const [strTry, setTry] = useState("");
      // const [user] = useState("Admin");
      const [display, setDisplay] = useState(true);
      

      const checkPass = async () =>{

        const user = JSON.parse(localStorage.getItem("user"));
        const body = {
          "email": user.email,
          "password": pass
        };

      const passwordCheck = await axios.post(`${API_Endpoint}/validate`, body,
      {
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      })
      console.log('password check', passwordCheck)
        if(passwordCheck && passwordCheck.data){
            setSuccess(true);
            setDisplay(false);
            props.authEditProfile(true);
            props.handleCheckClose(false);
        }
        else{
          setTry('Wrong password, try again !');
          setPass("");
          setDisplay(true);
        }
      }
        return (
          <Modal show={props.pswdCheckModal} onHide={props.handleCheckClose}>
            <Modal.Header closeButton>
              <Modal.Title><b>Authorize user to edit profile</b></Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {display && <div><p><center>{strTry}</center></p><form>
                <input type="password" value={pass} id="password" className="form-control" placeholder="Enter Password" onChange={(e)=>setPass(e.target.value)}></input>
                <br/>
                </form></div>}
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={checkPass} style={{color:"white" , backgroundColor: "#014E6A", borderColor: "#014E6A", fontSize: "14px"}} >Submit</Button>
            </Modal.Footer>
          </Modal>
        );
      }



export default PasswordCheck;
