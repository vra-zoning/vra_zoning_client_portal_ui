import  React, {react, useState, useEffect} from 'react';
import "./LicenseTypes.css";
import LicenseCard from './LicenseCard';
import {Card} from "react-bootstrap";

function SampleNextArrow(props) {
    const { className, style, onClick } = props;

    return (
      <div className={className} style={{ background: "#fff" }} onClick={onClick}>
        <img style={{ width: "200px" }} src="https://www.google.com/imgres?imgurl=https%3A%2F%2Fmedia.msufcu.org%2Fpublicsites%2Fpublicsite%2Ficons%2Farrow_right_gray.png&imgrefurl=https%3A%2F%2Fwww.oucreditunion.org%2Fpress_releases%2F&tbnid=JLmM6DfHjhUQgM&vet=12ahUKEwiQ28XJoLX4AhUEwykDHa1iCRgQMygsegUIARDfAQ..i&docid=vutHEDqZt4d15M&w=200&h=200&itg=1&hl=en-GB&ved=2ahUKEwiQ28XJoLX4AhUEwykDHa1iCRgQMygsegUIARDfAQ"/>
      </div>
    );
  
  }

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
  
    return (
      <div className={className} style={{ background: "#fff" }} onClick={onClick}>
        <img style={{ width: "200px" }} src="https://toppng.com/uploads/preview/arrow-pointing-to-the-left-115501167743epfu1fapc.png"/>
      </div>
    ); 
  }
  
   

const LicenseTypes = () =>{

    const [number, SetNumber] = useState("");
    const [name, SetName] = useState(""); 
    const [month, SetMonth] = useState(""); 
    const [memberSince, setMemberSince] = useState("");

    var settings = {
        className: "center",
        centerMode: true,
        centerPadding: "60px",
        dots: true,
        infinite: false,
        speed: 500,
        slidesToShow: 2,
        //slidesToScroll: 5,
        initialSlide: 0,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              //slidesToScroll: 3,
              dots: false
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              // slidesToScroll: 2,
              dots: false
            }
        }
        //   },
        //   {
        //     breakpoint: 480,
        //     settings: {
        //       slidesToShow: 1,
        //       // slidesToScroll: 1,
        //       dots: true
        //     }
        //   }
        ]
      };

    useEffect(() => {
        const DATA =JSON.parse(localStorage.user);
        SetName(DATA.firstName+" "+DATA.lastName);
        SetNumber(DATA.cusT_ID)
        setMemberSince(DATA.registered_on.split("T")[0].split("-")[0])
        // console.log("registered_on",memberSince)
    }, [])

    return(

        <div className="license-type-container">
            <div className="license-type-title">
                <h1><b>{name}'s License Type</b></h1>
            </div>

            <div className='license-type-subtitle'>
                <p>This page is to show you the specific licensing type associated with your active account.</p>
            </div>

            <div className="license-type-body">
                <div className='row'>
                   <LicenseCard customerID = {number} name={name} license="VRA Max" memberSince={memberSince}/>
                </div>
                <br></br>
                <br></br>
                <br></br>

                <div>
                <h4>Interested in learning about other VRA Licensing types? Check out the different VRA Licensing Types below:”</h4>
                </div>

                <br></br>

                <div className='license-body-grid'>

                <div className='license-cards'>

                {/* <Slider {...settings}> */}

                <Card className='demo-cards'>
                <div className='licence-types-card'>
                    <div className='license-card-alignment'>
                        <LicenseCard customerID = "01" name="Rob Christy" license="VRA Max" memberSince="2021"/>
                    </div>
                    <div className='license-description'>
                        <p>Expand your company research capabilities with VRA Max. 
                        A high-performance tool which allows companies to pay for every time they run a specific product,
                         such as, Infographics Site Summary, Geographic Site Summary, Planning and Zoning Plan Review, etc. 
                        </p>
                    </div>
                </div>
                </Card>

                <Card className='demo-cards'>
                <div className='licence-types-card'>
                    <div className='license-card-alignment'>
                        <LicenseCard customerID = "01" name="Rob Christy" license="VRA Bishop" memberSince="2021"/>
                    </div>
                    <div className='license-description'>
                       <p>
                       Expand your company research capabilities with VRA Bishop. A high-performance tool which allows companies to pay for a subscription 
                       to save more than $192,000.00 per every 3 months. With customizable features to be able to run as many Infographics Site Summary, 
                       Geographic Site Summary, Planning and Zoning Plan Review, etc., 
                       as necessary with customizable settings to make the subscription type specific for your company.
                       </p>
                    </div>
                </div>
                </Card>

                <Card className='demo-cards'>
                <div className='licence-types-card'>
                    <div className='license-card-alignment'>
                        <LicenseCard customerID = "01" name="Rob Christy" license="VRA Unlimited" memberSince="2021" />                    
                    </div>
                    <div className='license-description'>
                       <p>
                       Expand your company research capabilities with VRA Unlimited. A high-performance tool which allows companies to pay for a unlimited VRA Access. 
                       VRA Unlimited features to be able to run as many Infographics Site Summary, Geographic Site Summary, Planning and Zoning Plan Review, etc.
                       </p>
                    </div>
                </div>
                </Card>

                {/* </Slider> */}

            </div>
            </div>
        </div>
        </div>
    )
}


export default LicenseTypes;