
import { React, useState } from 'react';
import { Modal, Button } from "react-bootstrap";
import axios from 'axios';


const PasswordModal = (props) => {

  const API_Endpoint ="https://vrazoningclientws.azurewebsites.net"

  var [oPass, setOPass] = useState("");
  var [nPass, setNPass] = useState("");
  const [display, setDisplay] = useState(true);

  const checkAndSet = async () => {
  
    const user = JSON.parse(localStorage.getItem("user"));
    const body = {
      email: user.email,
      password: oPass
    };
    const passwordCheck = await axios.post(`${API_Endpoint}/validate`, body,
      {
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      })
    if (passwordCheck && passwordCheck.data) {
      const passChangeObj = {
        password: nPass
      };
     await axios.put(`${API_Endpoint}/api/Customers/${user.email}`, passChangeObj,
        {
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          },
        })
        .then((passwordChange) => {if(passwordChange && passwordChange.data && passwordChange.status === 200){ props.handleClose();}
                                    else{
                                      setDisplay(false);
                                    }
              })
        //else error message from backend. 
        //if not correct oPass, simply display an error message on the window and dont close the window
        .catch(error => {
          window.alert('There was an error!', error);
        })
    }
    setOPass("");
    setNPass("");
  }

  return (
    <Modal show={props.pswdModalOpen} onHide={() => { setDisplay(true); props.handleClose(); }}>
      <Modal.Header closeButton>
        <Modal.Title>Change Password</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {display && <form>
          <input type="password" className="form-control" name="oldpassword" placeholder="Enter Old Password" value={oPass} onChange={(e) => setOPass(e.target.value)} /><br />
          <input type="password" className="form-control" name="newpassword" placeholder="Enter New Password" value={nPass} onChange={(e) => setNPass(e.target.value)} /><br />
        </form>}
        {!display && "Wrong Old Password, try again later"}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={checkAndSet} style={{ color: "white", backgroundColor: "#014E6A", borderColor: "#014E6A", fontSize: "14px" }}>Change Password</Button>
      </Modal.Footer>
    </Modal>
  );
}



export default PasswordModal;
