import React from "react";
import "./Footer.css";
import Box from '@mui/material/Box'
import { useNavigate } from 'react-router-dom';

function Footer() {

  const navigate = useNavigate();
  var isAuthenticated = localStorage.getItem("user");

  const logout = () => {
    localStorage.clear();
    isAuthenticated = localStorage.getItem("user");
    navigate("/");
    window.location.reload();
  }

  return (
    <Box marginTop="3%">
      <footer className="footer mt-auto py-3 bg-light border-top">
        <div className="container">
          <div className="row py-3 border-bottom">
            <div className="col-md-3">
              <h5>Company</h5>
              <p>
                Virtual Review Assist
                <br />
                747 Sw 2nd Ave, STE 357, 358
                <br />
                Gainesville, FL 32601
              </p>
              <p>
                <a href="tel:813 376 3088">813-376-3088</a>
              </p>
            </div>
            <div className="col-md-6">
              <h5>Quick Links</h5>
              <ul>
                {!isAuthenticated ? <li>
                  <a href="/login">Account Login</a>
                </li> : <li><a onClick={() => { logout() }}>Logout</a></li>}

                <li>{isAuthenticated ? <a href="/DashboardHome">Home Page</a> : <a href="/login">Account Login</a>}</li>

                <li>
                  <a href="/AboutUs">Our Purpose</a>
                </li>
                <li>
                  <a href="/ContactUs">Contact Us</a>
                </li>
              </ul>
            </div>
            <div className="col-md-3">
              <h5>Find Us On</h5>
              <ul className="nav">
                <li>
                  <a>
                    <i className="fab fa-facebook-f" />
                  </a>
                </li>
                <li>
                  <a>
                    <i className="fab fa-twitter" />
                  </a>
                </li>
                <li>
                  <a>
                    <i className="fab fa-linkedin-in" />
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div className="row pt-3">
            <div className="col-12 text-center">
              <span className="text-muted">
                Copyright © 2021 VRA. All Rights Reserved.
              </span>
            </div>
          </div>
        </div>
      </footer>
    </Box>
  );
}

export default Footer;
