import React from "react";

class Transportation extends React.Component {

  render() {
    let transport1 = require("./transport1.png");
    let transport2 = require("./transport2.png");
    let transportreview = require("./transport3.png");
    return (
      <div>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1"
        />
        <title>Virtual Review Assist</title>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;500;700;900&display=swap"
          rel="stylesheet"
        />
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
        />
        <link rel="stylesheet" href="/css/bootstrap.min.css" />
        <link rel="stylesheet" href="/css/style.css" type="text/css" />
        {/* Below Files are for the File manager - Documentation can be found at https://plugins.krajee.com/file-input */}
        <link
          href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.2.2/css/fileinput.min.css"
          media="all"
          rel="stylesheet"
          type="text/css"
        />
       
          <div className="row" >
         
            <div className="col-md-1"></div>
            <div className="col-md-11">
                        <div style={{height:"50px", borderRadius:"6px", backgroundColor:"rgb(1, 78, 106)", color:"white", display:"flex", alignItems:"center", justifyContent:"center"}}>
                        <h2 style={{margin:"0"}}>
                        <center><b > Transportation Review </b></center>
                        </h2>
                        </div>
              <br></br>
              <i><p>Clients submit their reports to VRA Transportation Review and VRA reviews the AutoCAD File and generates Flag Notes. Flag Notes are the way that VRA tells the client what they need to fix and why they need to fix it, all according to the government entity standards. </p></i>
              <div className="row" >
                <i><p>The process would go as follows:</p></i>
                <div className="row">
                <center>
                  <img src={transportreview} alt="Transportation Review" style={{width:"70%", height: "100%" }}></img>
                  </center>
                </div>
                <div className="col-md-6">
                <i><p>VRA provides developers the opportunity to get their plans reviewed prior to submitting to the government entity. This enhances the developers’ chances of getting their site plan approval the first review cycle. The transportation review does consist of reviewing over 144 local transportation requirements for the improvement of a site. The transportation review is normally reviewed by multiple people in a government entity, these people could have different opinions with different approval conditions. </p></i>
                <i><p>VRA removes all biases and references only to the code specific requirements. The specific code requirements allow the developer to take a closer look at what should be fixed and why it should be fixed. After reviewing your AutoCAD file, VRA will give the client, “Flag Notes”. The “Flag Notes” are created based on what corrections must be made on your AutoCAD file and why they need to be corrected. </p></i>
                <i><p>Flag Notes always include a report showing you what specific sections in your site plan must be corrected based on the specific government entity code requirements. The Flag Notes will have a summary for every transportation attribute which is flagged and show the client any related code links to show the specific why, as to why the site plan must be corrected prior to approval.  </p></i>
                </div>
                <div className="col-md-3" >
                  <img src={transport1} alt="Transportation Review" style={{ height: "100%" }}></img>
                </div>
                <div className="col-md-3" >
                  <img src={transport2} alt="Transportation" style={{ height: "100%" }}></img>
                </div>
              </div>
              
            </div>

          </div>
        </div>
    );
  }
}

export default Transportation;
