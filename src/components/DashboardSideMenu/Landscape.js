import React from "react";


class Landscape extends React.Component{

    render() {
      let landscapeflow = require("./landscapeflow.png");
      let flagnotes1 = require("./flagnotes1.png");
      let flagnotes2 = require("./flagnotes2.png");
        return(
            <div>
                <meta
              name="viewport"
              content="width=device-width, initial-scale=1"
            />
            <title>Virtual Review Assist</title>
            <link rel="preconnect" href="https://fonts.googleapis.com" />
            <link
              rel="preconnect"
              href="https://fonts.gstatic.com"
              
            />
            <link
              href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;500;700;900&display=swap"
              rel="stylesheet"
            />
            <link
              rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
            />
            <link rel="stylesheet" href="/css/bootstrap.min.css" />
            <link rel="stylesheet" href="/css/style.css" type="text/css" />
            {/* Below Files are for the File manager - Documentation can be found at https://plugins.krajee.com/file-input */}
            <link
              href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.2.2/css/fileinput.min.css"
              media="all"
              rel="stylesheet"
              type="text/css"
            />
                

                <div className="row" >
                    <div className="col-md-1"></div>
                    <div className="col-md-11" >
                     
                        <div style={{height:"50px", borderRadius:"6px", backgroundColor:"rgb(1, 78, 106)", color:"white", display:"flex", alignItems:"center", justifyContent:"center"}}>
                        <h2 style={{margin:"0"}}>
                        <center><b > Landscape Review </b></center>
                        </h2>
                        </div>
                        <br></br>
                        <i><p>VRA has created a comprehensive landscape review to ensure your site plans are up to the Florida Landscaping Code and ANSI A300 Landscaping standards. </p></i>
                        <div className="row">
                        <div className="col-md-4" style={{left:"20%"}}>
                          <ul>
                            <i><li>Native Shade Trees</li>
                            <li>Native Accent Trees</li>
                            <li>Native Palm Species</li>
                            <li>Native Shrubs</li>
                            <li>Native Groundcover Species</li>
                            <li>Native Grass Species </li>
                            <li>Native Grass Species </li>
                            <li>Mulching</li></i>
                            <i><li>ROW Buffer</li>
                            <li>Interior Parking Guidelines</li>
                            <li>Street Trees</li>
                            <li>Buffers and Screening Requirements</li>
                            <li>Open Storage</li>
                            <li>Solid Waste Storage</li>
                            <li>Stormwater Evaluation</li>
                            <li>Wheel Stops and Curbs</li></i>
                          </ul>
                        </div>
                        <div className="col-md-3">
                          <div style={{height:"300px", width:"300px"}}>
                             <img src={flagnotes1} alt="Land Use Info" style={{height:"100%", width:"100%"}}></img>
                          </div>
                        </div>
                        <div className="col-md-4">
                          <div style={{height:"300px", width:"300px"}}>
                             <img src={flagnotes2} alt="Land Use Info" style={{height:"100%", width:"100%"}}></img>
                          </div>
                        </div>
                        </div>
                        <i><p>After VRA Reviews your CAD file attributes, VRA will give you results on what you need to fix and why you need to fix it, prior to submitting to a government entity. Reducing the amount of time, a client would have to go back and forth to the city prior to the approval of their site plan. The results are called Flag Notes, the Flag Notes will provide you with the information you need to adjust your file. Allowing the client to make sure their site plan is up to code prior to submitting to a government entity.</p></i>
                        <i><p>Flag Notes are the way that VRA tells the client which code they are not complying to and how to fix this error. Landscaping can sometimes become an overlooked hassle for developers to examine every detail of the specie type, buffering and screening requirements, or even the placement and location of the landscape implementation. Flag Notes show specifically how the developer can adjust their plans to the approval requirements and gives the developers the specific municide links of where to refer to the code for that specific code requirement. Giving the client the client access to the specific land development code, zoning code, or comprehensive plan links to understand the specific area that needs to be fixed and why it must be corrected prior to approval. </p></i>
                        <i><p>Government Entities do have their specific local Landscaping Code and Land Development Code Guidelines. VRA takes all the local code, Florida Landscaping Codes, and ANSI A300 landscaping requirements to review your site plan and make sure that it is ready for submittal according to those specific code requirements. The revitalization, preservation, and conservation of the current site is something that must be carefully looked at prior to submittal. The denial of a plan over tree placements shall be a problem no more. </p></i>
                        <i><p>Our Landscaping Review reviews the proposed pervious structures and the placement of each species. The codes regarding the previous structure and the new landscape define distances of location and placement of the landscaping standards for the specific site. VRA algorithms process thousands of codes to make sure that you have the support you need to show that you are following all government code requirements for the landscaping approval. Removing all political biases from the landscaping review process and solely referring to the specific code requirements for that zone.</p></i>
                        <i><p>Expedite your landscape review by using VRA software to verify the approval of your plans prior to submitting to any government entity. Use the Request a Quote Button to find out how VRA can help you save time and money. </p></i>
                        <div style={{height:"400px"}}>
                            <center>
                            <img src={landscapeflow} alt="flow" style={{width:"70%", height:"70%"}}></img>
                            </center>
                        </div>
                    
                    </div>
                   
                  </div>
                
            </div>
        );
    }
}

export default Landscape;
