import React from "react";

class SiteSummary extends React.Component{

    render() {

        return(
            <div>
                <meta
              name="viewport"
              content="width=device-width, initial-scale=1"
            />
            <title>Virtual Review Assist</title>
            <link rel="preconnect" href="https://fonts.googleapis.com" />
            <link
              rel="preconnect"
              href="https://fonts.gstatic.com"
              
            />
            <link
              href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;500;700;900&display=swap"
              rel="stylesheet"
            />
            <link
              rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
            />
            <link rel="stylesheet" href="/css/bootstrap.min.css" />
            <link rel="stylesheet" href="/css/style.css" type="text/css" />
            {/* Below Files are for the File manager - Documentation can be found at https://plugins.krajee.com/file-input */}
            <link
              href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.2.2/css/fileinput.min.css"
              media="all"
              rel="stylesheet"
              type="text/css"
            />
                
                <div className="row" >
                    {/* <img src={sitesummarybg} alt="Snow" style={{width:"100%", height:"100%"}}></img> */}
                    <div className="col-md-1"></div>
                    <div className="col-md-11" >
                        <div style={{height:"50px", borderRadius:"6px", backgroundColor:"rgb(1, 78, 106)", color:"white", display:"flex", alignItems:"center", justifyContent:"center"}}>
                        <h2 style={{margin:"0"}}>
                        <center><b > Site Summary</b></center>
                        </h2>
                        </div>
                        <br></br>
                        <i><p>The VRA Site Summary Review, consist of the automated results of what the site and surrounding characteristics of the site consist of, processed into an analytical review. This tells the client specifically what they can or cannot build with the current zoning and future land use specifications for that specific parcel. There are many different land development possibilities with any site and by knowing what the site is composed of will help define how the site can be retrofitted. </p></i>
                        <i><p>Comparing the local income and homestead/non homestead properties to tell you the estimation selling price of a single-family home. Reviewing your property and analyzing the possibilities of developing single family homes, multifamily homes, duplexes, condos, apartments, and more. Knowing how many units you could possibly build on a property is vital for the buyer of the property. VRA has density calculations which we have taught AI to analyze the property and inform the client within seconds, what they can build on the property, how high they can build, minimum lot requirements, and more. </p></i>
                        <i><p>Informative decision making in investing into vacant land can help the buyers of the land understand the property analytics. The Site Summary will tell the buyer specifically what the land is composed of such as soil types and the current species on the land. Identification of what the land is composed of is vital to site plan development, showing the buyer what they can currently use on the property and what they will need to buy for landscaping requirements. Conserving the current state of the site as much as possible and enhancing the current site features through the site plan design suggestions.</p></i>
                        <i><p>VRA Site Summary will also inform you about the local demographics of the area such as the local languages spoken, income per household, spending categories per household, households with disability ratings, and more. Our demographic analysis informs who the neighborhood is and who the developer would have as potential clients. Understanding the community and knowing their needs to make sure the development possibilities relate to the community needs. Helping you create a summarized plan on how you could possibly approach the site development for any specific address point. </p></i>
                    </div>
                   
                  </div>
                </div>
                
        );
    }
}

export default SiteSummary;
