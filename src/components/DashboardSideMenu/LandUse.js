import React from "react";


class LandUse extends React.Component{

    render() {

      let landuseinfo = require("./landuseinfo.png");
      let landuseflow = require("./landuseflow.png");

        return(
            <div>
                <meta
              name="viewport"
              content="width=device-width, initial-scale=1"
            />
            <title>Virtual Review Assist</title>
            <link rel="preconnect" href="https://fonts.googleapis.com" />
            <link
              rel="preconnect"
              href="https://fonts.gstatic.com"
              
            />
            <link
              href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;500;700;900&display=swap"
              rel="stylesheet"
            />
            <link
              rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
            />
            <link rel="stylesheet" href="/css/bootstrap.min.css" />
            <link rel="stylesheet" href="/css/style.css" type="text/css" />
            {/* Below Files are for the File manager - Documentation can be found at https://plugins.krajee.com/file-input */}
            <link
              href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.2.2/css/fileinput.min.css"
              media="all"
              rel="stylesheet"
              type="text/css"
            />
                
                <div className="row" >
                    <div className="col-md-1"></div>
                    <div className="col-md-11" >
                       
                        <div style={{height:"50px", borderRadius:"6px", backgroundColor:"rgb(1, 78, 106)", color:"white", display:"flex", alignItems:"center", justifyContent:"center"}}>
                        <h2 style={{margin:"0"}}>
                        <center><b > Project Landuse Information </b></center>
                        </h2>
                        </div>
                        <br></br>
                        <i><p>The comprehensive review of Project Land Use Information consists of review of 26 attributes. Automating the process of collecting data on your site and transforming the site address you have proposed to VRA’s AutoCAD format. The extraction of this information for your site will be developed based on your specific site regulations per the zoning code. The site address will be automatically compared to the zoning code for the specific city/county that the site address is associated with.</p></i>
                        <div className="row">
                        <div className="col-md-7">
                          <i><p>Getting the data to create a Project Land Use Chart is tedious, however, necessary for developers to get their site plans approved. VRA Software takes the government entity’s codes and processes them through our thousands of algorithms to provide the client the answers with their site development plan. Removing the consultation process which is many hours of back and forth with a consultant. Giving the Developer their answers in property analytics faster. Project Land Use Information is vital for a developer to show the government entity that they do know the current code requirements and are willing to comply with them. </p></i>
                          <i><p>After generating the attribute tables and understanding them, VRA can continue to show you the site planning possibilities through our other software integration, such as the Site Summary Analysis, Transportation Analysis, and more.</p></i>
                        </div>
                        <div className="col-md-5">
                          <div >
                             <img src={landuseinfo} alt="Land Use Info" style={{height:"100%", width:"100%"}}></img>
                          </div>
                        </div>
                        </div>
                        <br></br>
                        <i><h4>Project Land Use information is tedious to collect, save the hassle by subscribing to VRA Software today !</h4></i>
                    <div><center><img src={landuseflow} alt="Snow" style={{width:"60%", height:"60%"}}></img></center></div>
                    </div>
                   
                  </div>
                </div>
            
        );
    }
}

export default LandUse;
