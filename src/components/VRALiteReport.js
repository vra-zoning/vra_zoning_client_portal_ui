import "./VRALiteReport.css";
import React, { useState } from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMapMarkerAlt} from '@fortawesome/free-solid-svg-icons'
import {faEnvelope, faCircle} from "@fortawesome/free-solid-svg-icons";

const VRALiteReport = React.forwardRef((props, ref) => {

    const [siteAddress, setSiteAddress] = useState("3800 sw 34th Street, Gainesville, Florida, 32608")
    const [mailingAddress, setMailingAddress] = useState("3800 sw 34th Street, Gainesville, Florida, 32608")
    const [pcn, setPCN] = useState("1234567890-ABCDEFG")
    const [folio, setFolio] = useState("jhugjbk")
    const [acerage, setAcerage] = useState("68.38000 ")
    const [propertyUse, setPropertyUsage] = useState("STRING")
    const [propertyZoning, setPropertyZoning] = useState("STRING")
    const [platBook, setPlatBook] = useState("STRING")
    const [legalDescription, setLegalDescription] = useState("BEG AT N 1/4 COR SEC 15 TWNSHP 27 RGE 18 E SAME BEING S 1/4 COR SEC 10 TWNSHP 27 RGE 18 E THN N 00 DEG 48 MIN 13 SEC E ALG W LINE OF SW 1/4 SW 1/4 OF SE 1/4 OF SEC 10 699.50 FT TO NW COR OF SW 1/4 OF SW 1/4 OF SE 1/4 OF SEC 10 THN S 89 DEG 27 MIN 53 SEC E ALG N LINE 661.14 FT TO NE COR THN N 00 DEG 52 MIN 28 SEC E 199.94 FTTO PT OF INTERS OF S LINE OF LOT 1 THN S 89 DEG 28 MIN 58 SEC E 476.81 FT THN S 39 DEG 11 MIN 31 SEC W 58.49 FT THN S 10 DEG 22 MIN 44 SEC W 152.15 FT THN S 22 DEG 34 MIN 10 SEC E 227.06 FT THN S 15 DEG 41 MIN 21 SEC W 281.84 FT THN S 44 DEG 43 MIN 08 SEC W 264.17 FT THN S 30 DEG 46 MIN 53 SEC W 256.60 FT THN S 08 DEG 17 MIN 04 SEC W 54.94 FT THN S 14 DEG 26 MIN 21 SEC E 95.31 FT THN S 22 DEG 08 MIN 45 SEC E 70.16 FT THN S 16 DEG 17 MIN 44 SEC E 93.87 FT THN S 33 DEG 21 MIN 46 SEC E 88.84 FT THN S 40 DEG 16 MIN 22 SEC E 142.98 FT THN S 43 DEG 37 MIN 56 SEC E 169.90 FT THN S 70 DEG 29 MIN 44 SEC E 264.34 FT THN S 45 DEG 20 MIN 45 SEC E 65.15 FT THN S 30 DEG 56 MIN 55 SEC W 369.87 FT THN S 02 DEG 54 MIN 12 SEC W 218.91 FT THN S 03 DEG 29 MIN 13 SEC E 365.24 FT THN S 13 DEG 27 MIN 33 SEC E 61.67 FT THN S 27 DEG 04 MIN 34 SEC E 66.28 FT THN S 54 DEG 54 MIN 28 SEC E 40.73 FT TH S 09 DEG 02 MIN 22 SEC W 630.60 FT TO PT INTERS OF N ROW LINE OF CRYSTAL LAKE RD THN N 89 DEG 59 MIN 58 SEC W 594.08 FT THN N 50 DEG 30 MIN 38 SEC W 73.57 FT THN N 12 DEG 10 40 SEC W 2614.99 FT TO POB ")


    return(
        <div ref={ref} > 
            <div className='vra-lite-bg'>
                <div className="vra-lite-report-title">
                    VRA Lite Report
                </div>
                <div className="cards-container-x">
        
                        <div className='site-address'>
                            <FontAwesomeIcon icon={faMapMarkerAlt} size='2x' /><h4 style={{display:"inline"}}>{'  Site Address'}</h4>
                            <p>{siteAddress}</p>
                        </div>
                        <div className="mailing-address">
                            <FontAwesomeIcon icon={faEnvelope} size='2x' /><h4 style={{display:"inline"}}>{'  Mailing Address'}</h4>
                            <p>{mailingAddress}</p>
                        </div>
                        <div className="pcn-folio-acerage-container">
                            <div className="pcn">
                                <b>PCN</b>
                                <br></br>
                                {pcn}
                            </div>
                            {(folio != "") && <div className="folio">
                                <b>FOLIO</b>
                                <br></br>
                                {folio}
                            </div>}
                            <div className="acerage">
                                <b>ACERAGE</b>
                                <br></br>
                                {acerage}
                            </div>
                        </div>
                   
                        <div className="puse-pzoning-platbook">
                            <div className="p-use">
                                <b>PROPERTY USE</b>
                                <br></br>
                                {propertyUse}
                            </div>
                            <div className="p-zoning">
                                <b>PROPERTY ZONING</b>
                                <br></br>
                                {propertyZoning}
                            </div>
                            <div className="p-book">
                                <b>PLAT BOOK/ PAGE NUMBERS</b>
                                <br></br>
                                {platBook}
                            </div>
                        </div>
                    <div className="legal-description">
                        <b>LEGAL DESCRIPTION</b>
                        <br></br>
                        {legalDescription}
                    </div>
                    <div>
                        <b>SALES HISTORY</b>
                        <br></br>
                        {legalDescription}
                    </div>
                </div>
            </div>
        </div>
    );
})

export default VRALiteReport;