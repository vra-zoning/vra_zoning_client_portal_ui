function userSessionOut(user) {
  if (localStorage.getItem("user") !== null) {
    if (Date.now() - user.login > 86400000) { // 24*60*60*1000
      localStorage.clear();
      window.location.href = window.location.origin + "/login";
    }
  }
}

export default userSessionOut;