import React, { useState, useEffect } from "react";
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import Highcharts3d from "highcharts/highcharts-3d.js";
import axios from "axios";

Highcharts3d(Highcharts);

const HighBarChartGeo = (props) => {
    const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    const [options, setOptions] = useState({})
    const currentMonth = new Date().getMonth() + 1;
    const [chartData, setChartData] = useState([{ label: monthNames[currentMonth - 1], value: 0 }, { label: monthNames[currentMonth - 2], value: 0 }, { label: monthNames[currentMonth - 3], value: 0 }, { label: monthNames[currentMonth - 4], value: 0 }]);

    const getGeoCount = () => {
        getData(props.geoRecords)
    }

    useEffect(() => {
        getGeoCount();
    }, [props.geoRecords])

    const getData = (records) => {
        let dummyData = [...chartData];
        let months = [];
        let counts = [];
        records && records.data && records.data.map((record) => {
            dummyData.forEach((obj) => {
                months.push(obj.label);
                let tempCount = 0;
                if (monthNames[record.month - 1] === obj.label) {
                    tempCount = record.count;
                }
                counts.push(tempCount);
            })
        })
        if (months.length === 0) {
            dummyData.forEach((obj) => {
                months.push(obj.label);
                counts.push(0);
            });
        }
        const optionsObj = {
            chart: {
                backgroundColor: "rgba(255,255,255,0.4)",
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                renderTo: 'container',
                type: 'column',
                options3d: {
                    enabled: true,
                    alpha: 0,
                    beta: 0,
                    depth: 44,
                    viewDistance: 25
                },
                width: 400,
                height: 400
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Total Geographic Reports Generated by Month',
                style: {
                    fontWeight: 'bold',
                    fontSize: '18px',
                }
            },
            subtitle: {
                text: ''
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                },
                column: {
                    depth: 25
                }
            },
            xAxis: {
                categories: months.reverse()
            },
            series: [{
                showInLegend: false,
                data: counts.reverse()
            }]
        };
        setOptions(optionsObj);
    }

    return <div><HighchartsReact highcharts={Highcharts} options={options} /></div>;
}

export default HighBarChartGeo; 