import {React, useState} from 'react';
import Calendar from "react-calendar";
import "./Calendar1.css";

const Calendar1 = () =>{

    const [date, setDate] = useState(new Date());

    const onChange = date => {
        setDate(date);
    }

       return (

        <div style={{ borderRadius:"4px"}}>

        <Calendar onChange={onChange} value={date}></Calendar>

        </div>
       )

}

export default Calendar1;