import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import Highcharts3d from "highcharts/highcharts-3d.js";
Highcharts3d(Highcharts);


const HighPieChart = (props) => {
  const options = {
    chart: {
      backgroundColor: "rgba(0,0,0,0)",
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie',
      options3d: {
        enabled: true,
        alpha: 45,
        beta: 0,
        depth: 0,
        viewDistance: 0
      },
    },
    credits: {
      enabled: false
    },
    title: {
      text: `% Of Types Of Reviews`,
      style: {
        fontWeight: 'bold',
        fontSize: '18px',

      }
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
      point: {
        valueSuffix: '%'
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        depth: 35,
        innerSize: "60%",
        dataLabels: {
          enabled: false
        },
        showInLegend: true
      }
    },
    series: [{
      name: '% Of Reviews',
      colorByPoint: true,
      data: [{
        name: 'Zoning',
        y: 0
      }, {
        name: 'Landscape',
        y: 0
      }, {
        name: 'Transportation',
        y: 0
      }, {
        name: 'Geographic Site Summary',
        y: props.geoReportCount
      }, {
        name: 'Infographic Site Summary',
        y: props.infoReportCount
      }, {
        name: 'Nearby Restaurant Summary',
        y: props.nrReportCount
      }]
    }]
  };

  return <div><HighchartsReact highcharts={Highcharts} options={options} /></div>;
}

export default HighPieChart; 