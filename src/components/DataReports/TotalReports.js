import {react, useState} from "react";
import { Card } from "react-bootstrap";
import "./Stats.css";
import axios from 'axios';

const TotalReports = (props) =>{
    const totalCount = props.infoReportCount + props.geoReportCount + props.nrReportCount;

    return (
        <div style={{ borderRadius:"4px"}} >

        <Card className="card1" >
            <Card.Body>
            <Card.Title style={{fontSize:"15px"}}># Of Infographic Site Summary Reports</Card.Title>
            <Card.Text style={{fontSize:"20px", textAlign:"right"}}>
                <b>{props.infoReportCount}</b>
            </Card.Text>
            </Card.Body>
        </Card>
 
        <Card className="card1">
            <Card.Body>
            <Card.Title style={{fontSize:"15px"}}># Of Nearby Restaurant Summary Reports</Card.Title>
            <Card.Text style={{fontSize:"20px", textAlign:"right"}}>
                <b>{props.nrReportCount}</b>
            </Card.Text>
            </Card.Body>
        </Card>

        <Card className="card1" >
        <Card.Body>
            <Card.Title style={{fontSize:"15px"}}># Of Geographic Site Summary Reports</Card.Title>
            <Card.Text style={{fontSize:"20px", textAlign:"right"}}>
                <b>{props.geoReportCount}</b>
            </Card.Text>
            </Card.Body>
        </Card>

        <Card className="card1">
            <Card.Body>
            <Card.Title style={{fontSize:"15px"}}># Of Reports Generated In Total</Card.Title>
            <Card.Text style={{fontSize:"20px", textAlign:"right"}}>
                <b>{totalCount}</b>
            </Card.Text>
            </Card.Body>
        </Card>

        </div>

    );

}

export default TotalReports;