import React, { useState, useEffect } from 'react';
import TotalReports from './TotalReports.js';
import Calendar1 from './Calendar1.js';
import "./Stats.css";
import HighPieChart from "./HighPieChart";
import HighBarChartInfo from './HighBarChartInfo.js';
import HighBarChartGeo from './HighBarChartGeo.js';
import HighBarChartNR from './HighBarChartNR.js';
import axios from 'axios';

const DataReports = (props) => {
    const API_Endpoint = "https://vrazoningclientws.azurewebsites.net"
    const user = localStorage.getItem("user") && JSON.parse(localStorage.getItem("user"));
    const [infoReportCount, setInfoReportCount] = useState(0);
    const [geoReportCount, setGeoReportCount] = useState(0);
    const [nrReportCount, setNRReportCount] = useState(0);
    const [infoRecords, setInfoRecords] = useState({});
    const [geoRecords, setGeoRecords] = useState({});
    const [nrRecords, setNRRecords] = useState({});


    useEffect(() => async () => {
        var infoCount = 0, geoCount = 0, nrCount = 0;
        const info = axios.get(`${API_Endpoint}/infoReportCount/${user.email}`,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
            })
        const geo = axios.get(`${API_Endpoint}/geoReportCount/${user.email}`,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
            })
        const nr = axios.get(`${API_Endpoint}/nrReportCount/${user.email}`,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
            })
        Promise.all([info, geo, nr]).then(([infoCountRecords, geoCountRecords, nrCountRecords]) => {
            setInfoRecords(infoCountRecords);
            setGeoRecords(geoCountRecords);
            setNRRecords(nrCountRecords);
            infoCountRecords.data.map(object => (infoCount += object.count));
            setInfoReportCount(infoCount);
            geoCountRecords.data.map(object => (geoCount += object.count));
            setGeoReportCount(geoCount);
            nrCountRecords.data.map(object => (nrCount += object.count));
            setNRReportCount(nrCount);
        });
    }, []);

    return (
        <div >
            <div>
                <div style={{
                    borderRadius: "15px", backgroundPosition: 'center',
                    height: "1500px", backgroundColor: "#eaeaea"
                }}>
                    <br></br>
                    <br></br>
                    <div >
                        <center>
                            <h2 >
                                <b> Data Reports </b>
                            </h2>
                        </center>
                    </div>
                    <br></br>
                    <br></br>
                    <br></br>
                    <div className="row">
                        <div className="col-md-6" style={{ display: "flex", justifyContent: "center", alignContent: "center" }}>
                            <div style={{ width: "400px", height: "300px", display: "flex", borderRadius: "3px", justifyContent: "center", alignItems: "center" }}>
                                <div>
                                    <HighPieChart userEmail={user.email} infoReportCount={infoReportCount} geoReportCount={geoReportCount} nrReportCount={nrReportCount} />
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6" style={{ display: "flex", justifyContent: "center", alignContent: "center" }}>
                            <div style={{ width: "400px", height: "300px", display: "flex", borderRadius: "3px", justifyContent: "center", alignItems: "center" }}>
                                <TotalReports userEmail={user.email} infoReportCount={infoReportCount} geoReportCount={geoReportCount} nrReportCount={nrReportCount} />
                            </div>
                        </div>
                    </div>
                    <br></br>
                    <br></br>
                    <br></br>

                    <div className="row" >
                        <div className="col-md-6" style={{ display: "flex", justifyContent: "center", alignContent: "center" }}>
                            <div >
                                <HighBarChartInfo infoRecords={infoRecords} />
                            </div>
                        </div>
                        <div className="col-md-6" style={{ display: "flex", justifyContent: "center", alignContent: "center" }}>
                            <div>
                                <HighBarChartGeo geoRecords={geoRecords} />
                            </div>
                        </div>
                    </div>
                    <br></br>
                    <br></br>
                    <div className="row">
                        <div className="col-md-6" style={{ display: "flex", justifyContent: "center", alignContent: "center" }}>
                            <div>
                                <HighBarChartNR nrRecords={nrRecords}/>
                            </div>
                        </div>
                        <div className="col-md-6" style={{ display: "flex", justifyContent: "center", alignContent: "center" }}>
                            <Calendar1 />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default DataReports;