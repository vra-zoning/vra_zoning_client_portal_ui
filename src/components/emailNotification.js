import emailjs from '@emailjs/browser';

export const sendEmailNotification = (templateParams) => {
    const service_id = "service_2j5vjfp";
    const template_id = "template_tbo8hk2";
    const public_key = "XG4Urf4d-j-oCgKMo"
    emailjs.send(service_id, template_id, templateParams, public_key)
        .then(() => {
        }, (error) => {
            console.log(error.text);
        });
};

export const sendVerificationEmailNotification = (templateParams, callback) => {
    const service_id = "service_2j5vjfp";
    const template_id = "template_phtr6j6";
    const public_key = "XG4Urf4d-j-oCgKMo"
    emailjs.send(service_id, template_id, templateParams, public_key)
        .then(() => {
            callback();
        }, (error) => {
            callback();
            console.log(error.text);
        });
};
