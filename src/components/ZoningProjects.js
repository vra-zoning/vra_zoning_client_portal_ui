import {Modal, Button, Form, Row, Col, CloseButton} from "react-bootstrap";
import {useState} from 'react';
import uploadFileToBlob, { isStorageConfigured } from "./AzureBlob";
import { sendEmailNotification } from "./emailNotification";
import axios from "axios";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCloudArrowUp} from '@fortawesome/free-solid-svg-icons'
import "./ZoningProjects.css";

const ZoningProjects = (props) =>{

    const API_Endpoint ="https://vrazoningclientws.azurewebsites.net"

    //for react-form validation
    const [validated, setValidated] = useState(false);

    //form value variables
    const [projectName, setProjectName] = useState("")
    const [pcn, setPCN] = useState("")
    const [streetAddress, setStreetAddress] = useState("")
    const [city, setCity] = useState("")
    const [county, setCounty] = useState("")
    const [state, setState] = useState("")
    const [zip, setZip] = useState("")
    const [reviewType, setReviewType] = useState("")
    const [file, setFile] = useState(null)
    const [description, setDescription] = useState("")
    const [addressType, setAddressType] = useState("siteAddress")

    //fetching teh details from browser cache
    const user = localStorage.getItem("user") && JSON.parse(localStorage.getItem("user"));
    const Cust_id = user.cusT_ID? user.cusT_ID: user.cust_id;

    //azure blobpath
    const [blobLink, setBlobLink] = useState("")


    const handleSubmit = (event) => {
      const form = event.currentTarget;
      if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
      }
      else{
      event.preventDefault();
      event.stopPropagation();
      onFileUpload();
      }
      setValidated(true);
    };

    //when upload button is clicked
    const onFileUpload = async () =>{

        let blob_link
        let date1

        // *** UPLOAD TO AZURE STORAGE ***
        const path = user.email + "/" + new Date().getFullYear() + "/" + projectName + "/" + county + "/inputs/" + file.name;        
        blob_link = await uploadFileToBlob(file, user.email, projectName, `${streetAddress}, ${city}, ${county}, ${state}, ${zip}`, county, path);

        //bloblink and timestamp to store in the DB
        setBlobLink(blob_link);
        date1 = new Date();

        //updating the database
        const projDetails = await updateProjectTable(blob_link, date1);
        props.setRefresh(!props.refresh);

        //to send the encrypted data in the upload-reports URL
        let userParams = {
            project_name: projectName,
            site_name: `${streetAddress}, ${city}, ${county}, ${state}, ${zip}`,
            pcn: pcn,
            county: county,
            email: user.email,
            city: city,
            review: reviewType,
            description: description,
            project_id: projDetails.project_id
          }
          var CryptoJS = require("crypto-js");
          let en_userParams = CryptoJS.AES.encrypt(JSON.stringify(userParams), "vrazoning")
          let uplink = `${API_Endpoint}/upload/` + en_userParams.toString();

          let templateParams = {
            from_name: "VRA Customer Portal",
            to_name: "zoning team",
            user_name: user.name,
            message: "<div>Project Name: " + projectName + "<br>Site Name: " + `${streetAddress}, ${city}, ${county}, ${state}, ${zip}` + "<br>PCN: " + pcn+ "<br>County: " + county + "<br>Review Type: " + reviewType + "</div>",
            cc_candidates: "nitheeshabeeredd@virtualreviewassist.com;sdugan@virtualreviewassist.com;pz@virtualreviewassist.com;",
            upload_link: "<a href=" + uplink + "> Upload here!</a>"
          };
          sendEmailNotification(templateParams);

          // reset state/form
          setProjectName("")
          setPCN("")
          setStreetAddress("")
          setCity("")
          setCounty("")
          setState("")
          setZip("")
          setFile(null)
          setReviewType("")

          props.handleClose()
          window.alert("File Uploaded Successfully!")

    }

    //calling the API to save the data in the database table : VRA_ZoningProjects
    const updateProjectTable = async (path, date) => {
    const body = {
      "CUST_ID": Cust_id,
      "email": user.email,
      "city": city,
      "project_name": projectName,
      "county": county,
      "site_address": `${streetAddress}, ${city}, ${county}, ${state}, ${zip}`,
      "pcn": pcn,
      "review_type": reviewType,
      "time_stamp": date.toJSON(),
      "file_path": path,
      "status": "In Progress"//status has to be fetched from the azure blob
    }
    const response = await axios.post(`${API_Endpoint}/api/report`, body,
      {
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      })
    return response.data;
  };


    return(
        <div>
            <Modal 
                show={props.zoningModal} 
                onHide={props.handleClose}
                className="zoning-modal"
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                >
                <Modal.Header className="zoning-modal-header">
                    <Modal.Title><b>Planning and Zoning Uploads <FontAwesomeIcon icon={faCloudArrowUp} /></b></Modal.Title>
                    <CloseButton variant="white" onClick={(e) => props.handleClose(e)}/>
                </Modal.Header>

                <Modal.Body>
                <Form noValidate validated={validated} onSubmit={handleSubmit}>

                    <Row className="mb-3">
                    <Form.Group className="mb-3" data-type="horizontal">
                        <Form.Label className='zoning-form-label'>Identify site by:</Form.Label>
                        <Form.Check
                            type='radio'
                            label="Address"
                            name="Address Type"
                            value="siteAddress"
                            onChange={(e) => setAddressType(e.target.value)}
                            required
                            
                        />
                        <Form.Check
                            type='radio'
                            label="PCN"
                            name="Address Type"
                            value="pcn"
                            onChange={(e) => setAddressType(e.target.value)}
                            required
                        />
                        <Form.Control.Feedback type="invalid">
                            Please choose a Input Type.
                        </Form.Control.Feedback>
                    </Form.Group>
                    </Row>

                    <Row className="mb-3">
                    <Form.Group as={Col} md="12" controlId="validationCustom01">
                        <Form.Label className='zoning-form-label'>Project Name</Form.Label>
                        <Form.Control
                        required
                        type="text"
                        placeholder="Project Name"
                        value={projectName}
                        onChange={(e) => setProjectName(e.target.value)}
                        />
                        <Form.Control.Feedback type='invalid'>Please Name your project</Form.Control.Feedback>
                    </Form.Group>
                    </Row>

                    <Row className="mb-3">
                    <Form.Group as={Col} md="8" controlId="validationCustom02">
                        {(addressType == "siteAddress") ? 
                        <><Form.Label className='zoning-form-label'>Street Address</Form.Label>
                        <Form.Control
                        required
                        type="text"
                        placeholder="eg: 747 SW 2nd AVENUE STE 160"
                        value={streetAddress}
                        onChange={(e) => setStreetAddress(e.target.value)}
                        /></> : 
                        <><Form.Label className='zoning-form-label'>PCN</Form.Label>
                        <Form.Control
                        required
                        type="text"
                        placeholder="eg: A-10-30-18-3ZM-000006-00021.0"
                        value={pcn}
                        onChange={(e) => setPCN(e.target.value)}
                        /></>}
                        <Form.Control.Feedback type='invalid'>Please enter Street address</Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} md="4" controlId="validationCustom03">
                        <Form.Label className='zoning-form-label'>City</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="City"
                            value={city}
                            onChange={(e) => setCity(e.target.value)}
                            required
                        />
                        <Form.Control.Feedback type="invalid">
                            Please enter a City.
                        </Form.Control.Feedback>
                    </Form.Group>
                    </Row>

                    <Row className="mb-3">
                    <Form.Group as={Col} md="6" controlId="validationCustom04">
                        <Form.Label className='zoning-form-label'>County</Form.Label>
                        <Form.Control 
                        type="text" 
                        placeholder="County"
                        value={county}
                        onChange={(e) => setCounty(e.target.value)} 
                        required 
                        />
                        <Form.Control.Feedback type="invalid">
                        Please provide a County.
                        </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} md="3" controlId="validationCustom05">
                        <Form.Label className='zoning-form-label'>State</Form.Label>
                        <Form.Control 
                            type="text" 
                            placeholder="State"
                            value={state}
                            onChange={(e) => setState(e.target.value)} 
                            required 
                        />
                        <Form.Control.Feedback type="invalid">
                        Please provide a valid state.
                        </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} md="3" controlId="validationCustom06">
                        <Form.Label className='zoning-form-label'>Zip</Form.Label>
                        <Form.Control 
                            type="text" 
                            placeholder="Zip"
                            value={zip}
                            onChange={(e) => setZip(e.target.value)} 
                            required 
                        />
                        <Form.Control.Feedback type="invalid">
                        Please provide a valid zip.
                        </Form.Control.Feedback>
                    </Form.Group>
                    </Row>

                    <Form.Group className="mb-3">
                        <Form.Label className='zoning-form-label'>Review Type</Form.Label>
                        <Form.Check
                            type='radio'
                            label="Zoning"
                            name="Review Type"
                            value="Zoning"
                            onChange={(e) => setReviewType(e.target.value)}
                            required
                        />
                        <Form.Check
                            type='radio'
                            label="Landscape"
                            name="Review Type"
                            value="Landscaping"
                            onChange={(e) => setReviewType(e.target.value)}
                            required
                        />
                        <Form.Check
                            type='radio'
                            label="Transportation"
                            name="Review Type"
                            value="Transportation"
                            onChange={(e) => setReviewType(e.target.value)}
                            required
                        />
                        <Form.Control.Feedback type="invalid">
                        Please choose a Review Type.
                        </Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group controlId="formFile" className="mb-3">
                        <Form.Label className='zoning-form-label'>Upload your drawings here</Form.Label>
                        <Form.Control 
                            type="file"
                            onChange={(e) => setFile(e.target.files[0])} 
                            required
                        />
                        <Form.Control.Feedback type="invalid">
                        Please upload a file.
                        </Form.Control.Feedback>
                    </Form.Group>

                    <Form.Group md="12" controlId="validationCustom07" className="mb-3">
                        <Form.Label className='zoning-form-label'>Description</Form.Label>
                        <Form.Control 
                            type="textarea" 
                            placeholder="Your comments..." 
                            value={description}
                            onChange={(e) => setDescription(e.target.value)}
                        />
                    </Form.Group>

                    <Button type="submit" className="upload-button" >Upload</Button>
                </Form>  
                </Modal.Body>
          </Modal>
        </div>
    )
}
export default ZoningProjects;