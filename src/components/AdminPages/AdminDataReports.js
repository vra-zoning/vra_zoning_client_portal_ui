import { useState} from 'react';
import {Tabs, Tab} from "react-bootstrap";
import WebData from "./WebData.js";
import ClientAccounts from './ClientAccounts.js';
import ClientSummary from './ClientSummary.js';
import "./AdminDataReports.css";

const AdimnDataReports = () => {

    const [key, setKey] = useState('tab1');

    return (
        <div>

            <Tabs
                // id="controlled-tab-example"
                fill
                justify
                activeKey={key}
                onSelect={(k) => setKey(k)}
                className="tabs-class"
            >
                <Tab eventKey="tab1" title="Web Data">
                    <WebData />
                </Tab>

                <Tab eventKey="tab2" title="Client Accounts">
                   <ClientAccounts />
                </Tab>

                <Tab eventKey="tab3" title="Client Summary">
                    <ClientSummary />
                </Tab>

                <Tab eventKey="tab4" title="AutoCAD Submittals">
                    
                </Tab>


            </Tabs>
            
        </div>     
    )


}

export default AdimnDataReports;