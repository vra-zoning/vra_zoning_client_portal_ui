import "./AdminDataReports.css";

const WebData = () => {

    return(

        <div className='webdata-container'>

            <div className='row webdata-title'>
                <h1><b>WEB DATA COLLECTION</b></h1>
            </div>

            <div className='center-text-WebData'>

                <p>This section will be developed in the later stages</p>

            </div>

        </div>

    )
}

export default WebData;