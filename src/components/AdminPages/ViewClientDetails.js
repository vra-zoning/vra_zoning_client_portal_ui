import {Modal, Button} from "react-bootstrap";
import "./ViewClientDetails.css";
import HighPieChart from "../DataReports/HighPieChart.js";
import TotalReports from "../DataReports/TotalReports";

const ViewClientDetails = (props) =>{


    return(
        <div>
            <Modal 
                show={props.showClientDetailsModal} 
                onHide={props.handleClose}
                dialogClassName="client-details-modal"
                size="lg"
                aria-labelledby="example-modal-sizes-title-lg"
                scrollable={true}
                centered
                >
                <Modal.Header closeButton>
                    <Modal.Title><b>Client Account Details</b></Modal.Title>
                </Modal.Header>
                <Modal.Body className="view-details-body">

                    <div className="row">
                        <div className="col-md-6" >
                            <HighPieChart userEmail={props.userEmail}/>
                        </div>
                        <div className="col-md-6">
                            <TotalReports userEmail={props.userEmail}/>
                        </div>
                    </div>

                    <hr />

                    <div className="row">

                    </div>


                    
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={props.handleClose} style={{color:"white" , backgroundColor: "#014E6A", borderColor: "#014E6A", fontSize: "14px"}} >Close</Button>
                </Modal.Footer>
          </Modal>
        </div>
    )
}
export default ViewClientDetails;