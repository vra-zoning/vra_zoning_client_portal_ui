import { useState, useEffect} from 'react';
import { Card } from "react-bootstrap";
import "./AdminDataReports.css";
import axios from 'axios';
import ViewClientDetails from './ViewClientDetails';

const ClientAccounts = () => {

    //API string
    const API_Endpoint ="https://vrazoningclientws.azurewebsites.net"

    const [APIData, setAPIData] = useState([])
    const [searchValue, setSearchValue] = useState('');
    const [accountsStats, setAccountsStats] =useState([])

    const [alphabet, setAlphabet] = useState('');
    const alphabets = Array.from(Array(26).keys());

    const [showClientDetailsModal, setShowClientDetailsModal] = useState(false);

    const [userEmail, setUserEmail] = useState("")

    useEffect(() => {

        axios.get(`${API_Endpoint}/api/Customers`)
            .then((response) => {
                setAPIData(response.data);
            })
            axios.get(`${API_Endpoint}/api/Customers/GetCustomerCount`)
            .then((response) => {
                setAccountsStats(response.data);
            })
    }, [])

    return(

        <div className='webdata-container'>

            <div className='webdata-title'>
                <h1><b>CLIENT ACCOUNTS</b></h1>
            </div>

            <div className='row h-80'>
                <div className='client-search-bar' >
                <div className='col-md-8'>
                    <div className='client-search-letters'>

                    <div className='client-accounts-stats'>
                        <div className='client-accounts-stats-style'>
                            Total Accounts
                            <br></br>
                            <br></br>
                            <h4><b>{accountsStats[0]}</b></h4>
                        </div>

                        <div className='client-accounts-stats-style'>
                            This year
                            <br></br>
                            <br></br>
                            <h4><b>{accountsStats[1]}</b></h4>
                        </div>

                        <div className='client-accounts-stats-style'>
                            This Month                            
                            <br></br>
                            <br></br>
                            <h4><b>{accountsStats[2]}</b></h4>
                        </div>
                    </div>

                    <div className='reset-button-container'>
                    <button className="reset-button" type="button" onClick={(e) => {setAlphabet('');setSearchValue('');}}>Reset Search</button>
                    </div>

                    <div className='alphabets-container'>
                        {alphabets.map((i, index) => (<button key={`alpha-${index}`} className="alphabet-buttton" type="button" onClick={(e) => {setAlphabet(e.target.value)}} value={String.fromCharCode(i+65)} >{String.fromCharCode(i+65) }</button>))}
                    </div>

                    </div>
                </div>
                <div className='col-md-4'>
                    <div className="client-search-input">
                        <div>
                            <h3><b>Quick Search</b></h3>
                            <h6>Filter All Accounts</h6>
                            <input className="client-search-inputfield" type="text" placeholder='Start a Search...' value={searchValue} onChange={(e) => {setSearchValue(e.target.value)}} />
                            <i className="fa fa-search" aria-hidden="true"></i>
                        </div>   
                    </div>
                </div>
                </div>
            </div>

            <div className="cards-container">      
                {APIData
                        .filter((record) => (!searchValue.length && !alphabet.length)  ||  (alphabet.length && record.firstName
                            .toString()
                            .toLowerCase()
                            .startsWith(alphabet.toString().toLowerCase())) || (searchValue.length && record.firstName
                            .toString()
                            .toLowerCase()
                            .includes(searchValue.toString().toLowerCase()))
                        )
                        .map((user, i) => (
                            <Card key={`user-${i}`} className="card-style">
                                <Card.Body>
                                        <Card.Title className='card-title-clients'>{user.firstName}</Card.Title>
                                        <Card.Link onClick={()=>{setUserEmail(user.email);setShowClientDetailsModal(true);}} className='card-link-clients' href="#">View Details</Card.Link>
                                        <Card.Link className='card-link-clients' href="#">Add Notes</Card.Link>
                                </Card.Body>
                            </Card>
                ))}
            </div>

            {/* view client details modal */}
            {showClientDetailsModal && <ViewClientDetails showClientDetailsModal={showClientDetailsModal} handleClose={() => setShowClientDetailsModal(false)} userEmail={userEmail}></ViewClientDetails>}

        </div>

    )
}

export default ClientAccounts;