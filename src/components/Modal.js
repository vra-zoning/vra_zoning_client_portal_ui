import React from "react";

const Modal = ({ handleClose, show, children }) => {
  const showHideClassName = show ? "modal d-block" : "modal d-none";

  return (
    <div className="container">
    <div className="row">
      <div className="col-md-3">
    <div className={showHideClassName}>
      <div className="modal-container">
        {children}
       
      </div>
    </div>
    </div>
    </div>
    </div>
  );
};

export default Modal;