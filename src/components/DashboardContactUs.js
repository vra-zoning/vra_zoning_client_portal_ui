import React, { useState } from "react";
import "../App.css";
import Footer from "./Footer.js";


class DashboardContactUs extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      number: null,
      email: null,
      fullName: null,
      comments: null,
    };

    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.sendEmail = this.sendEmail.bind(this);
    this.fullnameHandle = this.fullnameHandle.bind(this);
    this.PhonHandle = this.PhonHandle.bind(this);
    this.emailHandle = this.emailHandle.bind(this);
    this.commendHandle = this.commendHandle.bind(this);
  }

  onFormSubmit(e) {
    e.preventDefault();
    this.sendEmail();
  }

  sendEmail = () => {
    var x;
    const formData = new FormData();
    formData.append("fullName", this.state.fullName);
    formData.append("subject", this.state.number);
    formData.append("emailId", this.state.email);
    formData.append("comments", this.state.comments);

    const requestOptions = {
      method: "POST",
      body: formData,
    };

    fetch("https://vrazoningazurews.azurewebsites.net/api/sendEmail/", requestOptions)
      .then((response) => response.json())
      .then((data) => {
        console.log("Sent email successfuylly");
      });

    console.log("came here " + this.reportData);
    this.isUploaded = true;
  };
  fullnameHandle = (e) => {
    e.preventDefault();
    this.state.fullName = e.target.value;
  };
  PhonHandle = (e) => {
    e.preventDefault();
    this.state.number = e.target.value;
  };
  commendHandle = (e) => {
    e.preventDefault();
    this.state.comments = e.target.value;
  };
  emailHandle = (e) => {
    e.preventDefault();
    this.state.email = e.target.value;
  };

  render() {
    return (
      <>
        <div>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <title>Virtual Review Assist</title>
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link
            href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;500;700;900&display=swap"
            rel="stylesheet"
          />
          <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
          />
          <link rel="stylesheet" href="/css/bootstrap.min.css" />
          <link rel="stylesheet" href="/css/style.css" type="text/css" />
          {/* HEADER */}
          <div>
            {/* <Navbar /> */}
          </div>
          {/* END HEADER */}
          {/* HERO */}
          <div
            className="sub-hero"
            style={{ backgroundImage: 'url("images/placeholder-banner.jpg")' }}
          >
            <div className="container col-xl-10 col-xxl-8 px-4 py-5">
              <div className="row align-items-center g-lg-5 py-5">
                <div className="col-12 text-center text-lg-start text-white">
                  {/* HERO CONTENT */}
                  <h1 className="display-4 fw-bold text-center mx-0">
                    Contact Us
                  </h1>
                  {/* END HERO CONTENT */}
                </div>
              </div>
            </div>
          </div>
          {/* END HERO */}
          {/* CONTENT */}
          <div className="container py-5">
            <div className="row align-items-center">
              <div className="col-md-8 offset-md-2 text-center">
                <h2 className="primary-color">Contact Us</h2>
                <p>
                  VRA’s advance technologies helps City/ County, Building
                  Developers, Architects, Engineers (structural, MEP),
                  Contractors, Owners, and Home Owners Associations. Contact us
                  for more information.
                </p>
                <hr className="smallbar primary-bg" />
              </div>
            </div>
            <div className="row pt-4">
              <div className="col-lg-6">
                <p>
                  VRA offers advanced technology solutions to help City/County
                  developers, architects, engineers, contractors, homeowners,
                  homeowner associations save time, money, resources and enhance
                  the safety and security of the built environment.
                </p>
              </div>
              <div className="col-lg-5 offset-lg-1">
                <form className="p-4 p-md-5 border rounded-3 bg-light">
                  <div className="form-floating mb-3">
                    <input
                      type="text"
                      className="form-control"
                      id="CFName"
                      placeholder="Full Name"
                      onChange={(e) => { this.fullnameHandle(e) }}
                    />
                    <label htmlFor="CFName">Full Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input
                      type="email"
                      className="form-control"
                      id="CFEmail"
                      placeholder="Email Address"
                      onChange={(e) => { this.emailHandle(e) }}
                    />
                    <label htmlFor="CFEmail">Email Address</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input
                      type="tel"
                      className="form-control"
                      id="CFPhone"
                      placeholder="Phone Number"
                      onChange={(e) => { this.PhonHandle(e) }}
                    />
                    <label htmlFor="CFPhone">Subject</label>
                  </div>
                  <div className="form-floating mb-3">
                    <textarea
                      className="form-control"
                      id="CFComments"
                      placeholder="Comments"
                      style={{ height: "200px" }}
                      defaultValue={""}
                      onChange={(e) => { this.commendHandle(e) }}
                    />
                    <label htmlFor="CFComments">Comments</label>
                  </div>
                  <button
                    className="btn btn-lg btn-primary"
                    onClick={(e) => { this.onFormSubmit(e) }}
                  >
                    Submit
                  </button>
                </form>
              </div>
            </div>
          </div>
          {/* MAP */}
          <div>
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d55462.91413844763!2d-82.36034035077896!3d29.678248668120656!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88e6202169bf2c2b%3A0x80e7795d28108d96!2sGainesville%2C%20FL!5e0!3m2!1sen!2sus!4v1625845529325!5m2!1sen!2sus"
              width="100%"
              height={400}
              style={{ border: 0, verticalAlign: "bottom" }}
              allowFullScreen
              loading="lazy"
            />
          </div>
          {/* END MAP */}
          {/* END CONTENT */}
          {/* FOOTER */}
          <Footer />
          {/* END FOOTER */}
        </div>
      </>
    );
  }
}
export default DashboardContactUs;
