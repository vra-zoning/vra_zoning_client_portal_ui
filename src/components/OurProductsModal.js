import { useState } from 'react';
import "./OurProductsModal.css";
import SSCardImage from "./SiteSummaryCardImage.png";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLongArrowRight, faScaleBalanced } from '@fortawesome/free-solid-svg-icons'
import SiteSummaryProjects from './SiteSummaryProjects.js';
import ZoningCardImage from "./ZoningCardImage.png";
import ZoningProjects from "./ZoningProjects.js"
import { Modal, Button, Card, CloseButton } from "react-bootstrap";

const OurProductsModal = (props) => {

    const [siteSummaryModal, setSiteSummaryModal] = useState(false)
    const [zoningModal, setZoningModal] = useState(false)

    //temp check
    const siteSummaryUpload = (projectName, county, siteName, projectId, bufferList, reviewType) => {
        props.siteSummaryUpload(projectName, county, siteName, projectId, bufferList, reviewType)
        //this.modalClose();
        // window.alert("Site Summary Report Generation is in Progress");
    }

    return (
        <div>
            {!siteSummaryModal && <div>
                <Modal
                    show={props.OurProductsModal}
                    onHide={props.handleClose}
                    dialogClassName="our-products-modal"
                    size="lg"
                    aria-labelledby="example-modal-sizes-title-lg"
                    scrollable={false}
                    centered
                >
                    <Modal.Header className='our-products-header'>
                        <Modal.Title className='our-products-title'><b>Our Products</b></Modal.Title>
                        <CloseButton variant="white" onClick={(e) => props.handleClose(e)} />
                    </Modal.Header>
                    <Modal.Body className="our-products-body">

                        <div class="row">
                            <div class="col-md-6" >
                                <Card className='our-products-card'>
                                    <Card.Img className='our-products-image' src={SSCardImage} />
                                    <Card.Body>
                                        <Card.Title>Site Summary Generation</Card.Title>
                                        <Card.Text className="sstext">
                                            Expand your companies' development possibilities with integration of a VRA Site Summary in your land planning practices.
                                            VRA's Planning and Zoning Site Summary generation gives clients the ability to choose one of three report types; Infographics Site Summary,
                                            Restaurents Site Summary Report and Geographic Site Summary Report.
                                        </Card.Text>
                                        <Card.Link className='card-link' variant="primary" onClick={() => setSiteSummaryModal(true)}>Start New Project   <FontAwesomeIcon icon={faLongArrowRight} /></Card.Link>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div class="col-md-6">
                                <Card className='our-products-card'>
                                    <Card.Img className='our-products-image' src={ZoningCardImage} />
                                    <Card.Body>
                                        <Card.Title>VRA Planning and Zoning Review</Card.Title>
                                        <Card.Text>
                                            Tired of going back and forth for Planning and Zoning approval? Get your plans reviewed by VRA's Planning and Zoning AI technology which
                                            compares your PDFs/CAD files with the city and county municipality codes for all over Florida!
                                        </Card.Text>
                                        <Card.Link className='card-link' variant="primary" onClick={() => setZoningModal(true)}>Start New Project   <FontAwesomeIcon icon={faLongArrowRight} /></Card.Link>
                                    </Card.Body>
                                </Card>
                            </div>
                        </div>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={props.handleClose} className="our-products-button">Close</Button>
                    </Modal.Footer>
                </Modal>
            </div>}
            {zoningModal && ( 
                <ZoningProjects 
                    zoningModal={zoningModal} 
                    handleClose={() => setZoningModal(false)} 
                    setRefresh={props.setRefresh}
                    refresh={props.refresh} 
                />)}
            {siteSummaryModal && (
                <SiteSummaryProjects 
                    siteSummaryModal={siteSummaryModal} 
                    handleClose={() => setSiteSummaryModal(false)}
                    setRefresh={props.setRefresh}
                    refresh={props.refresh} 
                />
                )}
        </div>
    )
}
export default OurProductsModal;