import React, { useState } from "react";
import "../App.css";
import Footer from "./Footer.js";


function DashboardAboutUs() {
  return (
    <>
      <div>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Virtual Review Assist</title>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com"  />
        <link
          href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;500;700;900&display=swap"
          rel="stylesheet"
        />
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
        />
        <link rel="stylesheet" href="/css/bootstrap.min.css" />
        <link rel="stylesheet" href="/css/style.css" type="text/css" />
        {/* HEADER */}
        <div>
          {/* <Navbar /> */}
        </div>
        {/* END HEADER */}
        {/* HERO */}
        <div
          className="sub-hero"
          style={{ backgroundImage: 'url("images/placeholder-banner.jpg")' }}
        >
          <div className="container col-xl-10 col-xxl-8 px-4 py-5">
            <div className="row align-items-center g-lg-5 py-5">
              <div className="col-12 text-center text-lg-start text-white">
                {/* HERO CONTENT */}
                <h1 className="display-4 fw-bold text-center mx-0">
                  Our Purpose
                </h1>
                {/* END HERO CONTENT */}
              </div>
            </div>
          </div>
        </div>
        {/* END HERO */}
        {/* CONTENT */}
        <div className="container py-5">
          <div className="row align-items-center">
            <div className="col-md-8 offset-md-2 text-center">
              <h2 className="primary-color">Purpose of VRA</h2>
              <p>"Life cycle viewpoint of your built environment"</p>
              <hr className="smallbar primary-bg" />
            </div>
          </div>
          <div className="row pt-4">
            <div className="col-md-12">
              <p>
                Building design and engineering is a creative yet technical
                process that involves numerous stakeholders working simultaneously
                using considerable monetary and human resources. In today's
                increased pace of construction activities in the U.S., it is vital
                that these technical drawings are reviewed and site-verified with
                utmost concentration upholding the building codes for the safety
                of the citizens.
              </p>
              <p>
                At Virtual Review Assist, Inc., we untangle the complex system of
                building review process with state-of-the-art patent-pending
                technologies developed from over 75 years of professional
                experience in the field of building code review, licensed
                architects, and engineers. Our purpose is to transform the way
                code compliance checkings are reviewed using our patent-pending
                technologies. We work alongside local governments, building
                developers, designers, and engineers for rapid, uniform,
                comprehensive, and consistent code compliance reviews.
              </p>
            </div>
          </div>
          <div className="row align-items-center">
            <div className="col-md-8 offset-md-2 text-center">
              <hr className="smallbar primary-bg" />
            </div>
          </div>
          <div className="row pt-1">
            <div className="col-12 col-md-6 col-lg-3 pb-4 text-center">
              <a href="#Rob">
                <img src="images/Rob.PNG" />
                <h3 className="h5 pt-1">Rob christy </h3>
                <div className="profile-title">CEO (Co-founder)</div>
                <div className="profile-email">
                  <button  style={{background:"transparent", border:"white",textDecoration:"underline"}}href="rob@virtualreviewassist.com">
                    rob@virtualreviewassist.com
                  </button>
                </div>
              </a>
            </div>

            <div className="col-1 col-md-1 col-lg-1 pb-4 text-center"></div>
            <div className="col-12 col-md-6 col-lg-3 pb-4 text-center">
              <a href="#Ravi">
                <img src="images/RaviRavi.png" />
                <h3 className="h5 pt-1">Dr.Ravi Srinivasan </h3>
                <div className="profile-title">Co-founder</div>
                <div className="profile-email">
                  <button style={{background:"transparent", border:"white",textDecoration:"underline"}} href="mailto:F.LName@email.com">
                    sravi@virtualreviewassist.com
                  </button>
                </div>
              </a>
            </div>

            <div className="col-1 col-md-1 col-lg-1 pb-4 text-center"></div>
            <div className="col-12 col-md-6 col-lg-3 pb-4 text-center">
              <a href="#Nawari">
                <img
                  width="650"
                  height=" 450"
                  resizemode="cover"
                  src="images/NawariNawari.jpg"
                />
                <h3 className="h5 pt-1">Dr. Nawari Nawari </h3>
                <div className="profile-title">Co-founder</div>
                <div className="profile-email">
                  <button style={{background:"transparent", border:"white",textDecoration:"underline"}} href="mailto:F.LName@email.com">
                    nawari@virtualreviewassist.com
                  </button>
                </div>
              </a>
            </div>
          </div>
          <div className="row align-items-center">
            <div className="col-md-8 offset-md-2 text-center">
              <hr className="smallbar primary-bg" />
            </div>
          </div>
          <div className="row py-3 align-items-center ">
            <div className="col-md-6">
              <img src="images/Rob.PNG" />
            </div>
            <div id="Rob" className="col-md-6">
              <h3>Rob Christy </h3>
              <p>
                Rob Christy has devoted the last 20 years working diligently to
                improve the construction service industry. Rob is the owner of one
                of the largest permit service firms in the United States, Suncoast
                Permits, LLC. Since its purchase in 2003, company estimates
                reflect they have processed approximately 100,000 building
                construction permits. Under his direction, the company has grown
                revenue by over 10 times while becoming the premier construction
                permit service in the industry. Solid leadership helped Rob grow
                his company, with a primary focus on teambuilding, mentoring,
                networking and sales development. Many of the largest residential
                and commercial builders utilize his knowledge and expertise to
                avoid permit review and construction delays. National single
                family home builders such as Lennar, Pulte Group, Toll Brothers
                and many more confidently place yearly business plans and
                projections in the hands of Rob because of his ability to meet
                deadlines, overcome complications, and recognize government
                inefficiency. He is a proud member of the Florida Home Builders
                Association and Tampa Bay Builders Association. While spending
                thousands of hours in the various building departments across the
                state of Florida, Rob studied the Florida Building Code and the
                many inefficient processes that often impede building permit plan
                review. He determined that improved building permit plan review
                technology was vital to the overall health of the construction
                industry. The need became obvious; help mitigate risk for builders
                and developers by creating products that accurately estimate
                costs, project starts, and allow for more rapid completion of
                building construction projects. Rob’s enthusiasm to improve
                outdated systems and processes will help drive Virtual Review
                Assist, Inc toward creating the most innovative products for the
                construction industry for years to come.
              </p>
            </div>
          </div>
          <div className="row py-3 align-items-center ">
            <div className="col-md-6 order-md-2">
              <img src="images/RaviRavi.png" />
            </div>
            <div id="Ravi" className="col-md-6 order-md-1">
              <h3>Dr. Ravi Srinivasan</h3>
              <p>
                Dr. Ravi Shankar Srinivasan is Holland Professor and University of
                Florida (UF) Term Professor, and Director of Graduate Programs and
                Research at M.E. Rinker, Sr. School of Construction Management,
                University of Florida (UF), Gainesville, Florida. He is the
                Director of UrbSys (Urban Building Energy, Sensing, Controls, Big
                Data Analysis, and Visualization) Lab at the Rinker School. He
                holds an M.S. degree in Civil Engineering from UF; and M.S. and
                Ph.D. degrees in Architecture (Building Technology) from the
                University of Pennsylvania, Philadelphia, Pennsylvania. He is a
                Certified Energy Manager, LEED Accredited Professional, and Green
                Globes Professional. He is an external faculty collaborator at the
                Center for Environmental Building & Design, School of Design,
                University of Pennsylvania. Dr. Srinivasan has practiced as a
                licensed architect in India, as an architect/ project manager in
                Singapore, and as an associate engineer in the U.S. prior to
                pursuing a career in academia. Dr. Srinivasan has published one
                book as a lead author titled, “The Hierarchy of Energy in
                Architecture: Emergy Analysis,” Routledge and co-edited a book
                titled, “Smart Cities: Foundations, Principles, and Applications,”
                John Wiley & Sons Inc. He has published over 100 techincal
                articles including journal articles, book chapters, and conference
                papers which has gained over 2,300 citations and an h-index of 22.
                He is a board member at the National Fenestration Rating Council
                (NFRC)and the Chair of ASHRAE Handbook subcommittee TC4.05
                Fenestration. More information is available at 
                <a href=" https://built-ecologist.com/">
                  https://built-ecologist.com/.
                </a>
              </p>
            </div>
          </div>
          <div className="row py-3 align-items-center ">
            <div className="col-md-6">
              <img src="images/NawariNawari.jpg" />
            </div>
            <div id="Nawari" className="col-md-6">
              <h3>Dr. Nawari Nawari</h3>
              <p>
                Dr. Nawari is a professor and University of Florida (UF) Term
                Professor in the College of Design, Construction, and Planning
                (DCP), School of Architecture, University of Florida. He served as
                the Assistant Dean for Graduate Education and currently serves as
                the Diversity officer for the College of Design, Construction, and
                Planning (DCP), UF. Dr. Nawari has written and co-authored six
                books and over 150 publications and advised more than 90 Master
                and Ph.D. Students. Dr. Nawari research focuses on Building
                Information Modeling (BIM), structural design, BIM
                standardization, automating building code conformance checking,
                and Blockchain Technologies. He is a frequent contributor to and
                invited speaker at national and international conferences. Dr.
                Nawari has contributed to the design profession with several
                innovations (e.g., the Structure and Architecture Synergy (SAS)
                Framework and the Generalized Adaptive Framework (GAF)) and during
                his career. Nawari’s works open the door to new teaching paradigms
                and design building structures using the Structure and
                Architecture Synergy (SAS) Framework. He is a member of the BIM
                committee of the Structural Engineering Institute (SEI) and
                co-chaired the subcommittee on BIM in education.For over 20 years,
                Dr. Nawari is a board-certified professional engineer in Florida
                and Ohio, with significant contributions to the practice of
                building design, safety, sustainability, and resilience. Notably,
                Dr. Nawari was inducted as a fellow of the American Society of
                Civil Engineers (ASCE) in 2016 for sustaining records of
                contributions to the field.
              </p>
            </div>
          </div>
        </div>
        {/* END CONTENT */}
        {/* FOOTER */}
        <Footer />
        {/* END FOOTER */}
      </div>
    </>
  );
}

export default DashboardAboutUs;
