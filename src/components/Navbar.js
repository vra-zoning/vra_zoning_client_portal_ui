import React, { useState } from "react";
import { useNavigate } from 'react-router-dom';
import QuoteModal from "./QuoteModal";
import { NavLink } from "react-router-dom";


export default function NavMenu() {
  const navigate = useNavigate();
  var isAuthenticated = localStorage.getItem("user");
  const user = {};
  const logout = () => {
    localStorage.clear();
    isAuthenticated = localStorage.getItem("user");
    navigate("/");
    window.location.reload();
  }

  let logo = require("./images/vra_logo.png");

  var [modalOpen, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  var [navScreen, setNavScreen] = useState("dashboard")


  return (
    // <HashRouter>
    <div>

      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>Virtual Review Assist</title>
      <link rel="preconnect" href="https://fonts.googleapis.com" />
      <link rel="preconnect" href="https://fonts.gstatic.com"  />
      <link
        href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;500;700;900&display=swap"
        rel="stylesheet"
      />
      <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
      />
      <link rel="stylesheet" href="/css/bootstrap.min.css" />
      <link rel="stylesheet" href="/css/style.css" type="text/css" />
      {/* Below Files are for the File manager - Documentation can be found at https://plugins.krajee.com/file-input */}
      <link
        href="https://cdn.jsdelivr.net/gh/kartik-v/bootstrap-fileinput@5.2.2/css/fileinput.min.css"
        media="all"
        rel="stylesheet"
        type="text/css"
      />
      {/* HEADER */}
      {/* HEADER */}
      <header className="p-3 border-bottom bg-light text-white">
        <div className="container">
          <div className="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
            {/* LOGO */}
            <div style={{ width: "80px", height: "80px" }}>
              <a
                href="/"
                className="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none main-logo"
              >
                <img src={logo} style={{ maxWidth: "100%", maxHeight: "100%" }} />
              </a>
            </div>
            {/* NAVIGATION */}
            <nav className="nav col-12 justify-content-center col-lg-auto m-auto mx-lg-4 me-lg-auto">
              <ul className="nav mb-2 justify-content-center mb-md-0">
                <li>
                  <NavLink to="/DashboardHome">Home</NavLink>
                </li>
                <li>
                  <NavLink to="/AboutUs">Our Purpose</NavLink>
                </li>
                {isAuthenticated && <li>
                  <NavLink to="/Dashboard">Dashboard</NavLink>
                </li>}
                {isAuthenticated && <li>
                  <a onClick={handleShow}
                    style={{ color: "#014E6A", cursor: "pointer" }}>
                    Request-a-Quote
                  </a>
                  {modalOpen && <QuoteModal
                    modalOpen={true}
                    handleClose={handleClose}
                  />}
                </li>}
                <li>
                  <NavLink to="/ContactUs">Contact Us</NavLink>
                </li>
              </ul>
            </nav>
            {/* LOGIN BUTTON */}
            {isAuthenticated && (
              <div className="text-end">
                <a href="/UserProfile" className="profile-link">
                  <span className="profile-img profile-small">
                    <img src={user.picture} />
                  </span>
                  {user.name}
                </a>
                <a
                  onClick={logout}
                  className="btn me-2 primary-border-color primary-color"
                >
                  <i className="fas fa-sign-out-alt" />
                  Sign Out
                </a>
              </div>
            )}
            {!isAuthenticated && (
              <div className="text-end">
                <a
                  onClick={() => navigate('/login')}
                  className="btn me-2 primary-border-color primary-color"
                >
                  <i className="fas fa-user-plus" />
                  Sign Up
                </a>
                <a
                  onClick={() => navigate('/login')}
                  className="btn me-2 btn-primary primary-border-color  dasdsa  primary-color"
                >
                  <i className="fas fa-user-circle" />
                  Login
                </a>
              </div>
            )}
          </div>
        </div>
      </header>
      {/* END HEADER */}
      {/* END HEADER */}
    </div>
  );
}
