import {Modal, Button, Form, Row, Col, CloseButton} from "react-bootstrap";
import { useState, useEffect } from 'react';
import Select from 'react-select';
import axios from "axios";
import uploadFileToBlob, { isStorageConfigured } from "./AzureBlob";
import "./SiteSummaryProjects.css"

const SiteSummaryProjects = (props) => {

    const API_Endpoint = "https://vrazoningclientws.azurewebsites.net"

    //for react-form validation
    const [validated, setValidated] = useState(false);
    const user = localStorage.getItem("user") && JSON.parse(localStorage.getItem("user"));
    const Cust_id = user.cusT_ID ? user.cusT_ID : user.cust_id;

    //form value variables
    const [projectName, setProjectName] = useState("")
    const [streetAddress, setStreetAddress] = useState("")
    const [city, setCity] = useState("")
    const [county, setCounty] = useState("")
    const [state, setState] = useState("")
    const [zip, setZip] = useState("")
    const [reviewType, setReviewType] = useState("")
    const [pcn, setPCN] = useState("");
    const [addressType, setAddressType] = useState("siteAddress")
    const [file, setFile] = useState(null)
    const [description, setDescription] = useState("")
    var [selectedBuffers, setSelectedBuffers] = useState([""]);
    const [blobLink, setBlobLink] = useState("");
    // const [refresh, setRefresh] = useState([false]);


    const bufferdata = [
        {
            value: 1,
            label: "0.5 mi"
        },
        {
            value: 2,
            label: "1 mi"
        },
        {
            value: 3,
            label: "2 mi"
        },
        {
            value: 4,
            label: "3 mi"
        },
        {
            value: 5,
            label: "5 mi"
        },
        {
            value: 6,
            label: "10 mi"
        },
        {
            value: 7,
            label: "20 mi"
        },
        {
            value: 8,
            label: "30 mi"
        },
        {
            value: 9,
            label: "40 mi"
        }
    ];

    const handleChange = (e) => {

        if (e.length < 5) {
            setSelectedBuffers(Array.isArray(e) ? e.map(x => x.value) : [""]);
        } else {
            alert('Please select only four Buffer Zones');
        }
    }

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
        }
        else {
            event.preventDefault();
            event.stopPropagation();
            onFileUpload();
        }
        setValidated(true);
    };

    const onFileUpload = async (e) => {

        const response = await axios.get(`${API_Endpoint}/Licenses/ValidateLicense?cust_id=${Cust_id}`,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
            })
        if (response.data === ("Invalid" || "User not found")) {
            window.alert("You don't have a Valid License . Contact Admin!!! You need Valid License to generate this Site Summary.")
        } else {
            let blob_link
            let date1

            if (reviewType === "ISiteSummary" || reviewType === "NRSiteSummary") {
                window.alert("Site Summary Report Generation is in Progress");
                const path = user.email + "/" + new Date().getFullYear() + "/" + projectName + "/" + county + "/inputs/site summary";
                blob_link = await uploadFileToBlob(new File([`${user.email}'s dummy site summary doc`], "dummy.txt", { type: "text/plain" }), "", "", "", "", path);

                //to save the blob path  and time in the database
                setBlobLink(blob_link);
                date1 = new Date()

                const projectDetails = await updateProjectTable(blob_link, date1);
                props.setRefresh(props.refresh+1);
                let radii = "";
                for (let r in selectedBuffers) {
                    radii += bufferdata[selectedBuffers[r] - 1].label.split(" ")[0] + ",";
                }
                radii = radii.substring(0, radii.length - 1);
                radii += ""
                var nums = Array.from(radii.split(','), Number);
                nums.sort(function (a, b) {
                    return a - b;
                });

                let numsstr = String(nums);
                numsstr = "[" + numsstr + "]";
                if (addressType === "siteAddress") {
                    siteSummaryUpload(projectName, county, `${streetAddress}, ${city}, ${county}, ${state}, ${zip}`, projectDetails.project_id, numsstr, reviewType, addressType);
                }
                else if (addressType === "pcn") {
                    siteSummaryUpload(projectName, county, `${pcn}`, projectDetails.project_id, numsstr, reviewType, addressType);
                }
            }

        }
        // reset state/form
        setProjectName("")
        setPCN("")
        setStreetAddress("")
        setCity("")
        setCounty("")
        setState("")
        setZip("")
        setReviewType("")
        props.handleClose()
        // window.location.reload();

    };

    const siteSummaryUpload = (projectName, county, siteName, projectId, bufferRadiiList, reviewType, addressType) => {
        const site_name = siteName.replace(" ", "%20").replace(",", "%2C");
        var request = new XMLHttpRequest();
        document.getElementById("controlled-tab-tab-Inprogress").click();
        if (addressType === "siteAddress") {
            if (reviewType === "ISiteSummary") {
                console.log("address iss ssp")
                request.open("GET", `${API_Endpoint}/api/Demography/GetDemographyReport?address=${site_name}&file_path=${user.email + "/" + new Date().getFullYear() + "/" + projectName + "/" + county + "/outputs/"}&project_id=${projectId}&county=${county}&bufferRadii=${bufferRadiiList}`, true);
            }
            else if (reviewType === "NRSiteSummary") {
                console.log("address nrs ssp")
                request.open("GET", `${API_Endpoint}/api/NearbyRestaurants/GetNearbyRestaurantsReport?address=${site_name}&file_path=${user.email + "/" + new Date().getFullYear() + "/" + projectName + "/" + county + "/outputs/"}&project_id=${projectId}&county=${county}&bufferRadii=${bufferRadiiList}`, true);
            }
        }
        else if(addressType==="pcn")
        {
            if (reviewType === "ISiteSummary") {
                console.log("pcn iss ssp")
                request.open("GET", `${API_Endpoint}/api/Demography/GetDemographyReport?pcnno=${site_name}&file_path=${user.email + "/" + new Date().getFullYear() + "/" + projectName + "/" + county + "/outputs/"}&project_id=${projectId}&county=${county}&bufferRadii=${bufferRadiiList}`, true);
            }
            else if (reviewType === "NRSiteSummary") {
                console.log("pcn nrs ssp")
                request.open("GET", `${API_Endpoint}/api/NearbyRestaurants/GetNearbyRestaurantsReport?pcnno=${site_name}&file_path=${user.email + "/" + new Date().getFullYear() + "/" + projectName + "/" + county + "/outputs/"}&project_id=${projectId}&county=${county}&bufferRadii=${bufferRadiiList}`, true);
            }
        }
        request.responseType = "blob";
        request.onload = async function (e) {
            if (this.status === 200) {
                props.setRefresh(props.refresh+2);
            } else {
                props.setRefresh(props.refresh+2)
                axios.put(`${API_Endpoint}/api/report/${projectId}`, { status: "Error" },
                    {
                        headers: {
                            'Content-Type': 'application/json',
                            'Accept': 'application/json',
                        },
                    })
            }
        };
        request.send();
    }

    const updateProjectTable = async (path, date) => {
        const body = {
            "CUST_ID": Cust_id,
            "email": user.email,
            "city": city,
            "project_name": projectName,
            "county": county,
            "site_address": `${streetAddress}, ${city}, ${county}, ${state}, ${zip}`,
            "pcn": pcn,
            "review_type": reviewType,
            "time_stamp": date.toJSON(),
            "file_path": path,
            "status": "In Progress"//status has to be fetched from the azure blob
        }
        const response = await axios.post(`${API_Endpoint}/api/report`, body,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
            })
        return response.data;
    };


    return (
        <div>
        <Modal 
            show={props.siteSummaryModal} 
            onHide={props.handleClose}
            dialogClassName="our-products-modal"
            size="lg"
            aria-labelledby="example-modal-sizes-title-lg"
            scrollable={false}
            centered
            >
            <Modal.Header className='ssummary-modal-header'>
                <Modal.Title><b>Site Summary Generation</b></Modal.Title>
                <CloseButton variant="white" onClick={(e) => props.handleClose(e)}/>
            </Modal.Header>
            <Modal.Body>
            <Form noValidate validated={validated} onSubmit={handleSubmit}>

            <Row className="mb-3">
                    <Form.Group className="mb-3" data-type="horizontal">
                        <Form.Label className='zoning-form-label'>Identify site by:</Form.Label>
                        <Form.Check
                            type='radio'
                            label="Address"
                            name="Address Type"
                            value="siteAddress"
                            onChange={(e) => setAddressType(e.target.value)}
                            required
                            
                        />
                        <Form.Check
                            type='radio'
                            label="PCN"
                            name="Address Type"
                            value="pcn"
                            onChange={(e) => setAddressType(e.target.value)}
                            required
                        />
                        <Form.Control.Feedback type="invalid">
                            Please choose a Input Type.
                        </Form.Control.Feedback>
                    </Form.Group>
                </Row>

                <Row className="mb-3">
                    <Form.Group as={Col} md="12" controlId="validationCustom01">
                        <Form.Label className='zoning-form-label'>Project Name</Form.Label>
                        <Form.Control
                        required
                        type="text"
                        placeholder="Project Name"
                        value={projectName}
                        onChange={(e) => setProjectName(e.target.value)}
                        />
                        <Form.Control.Feedback type='invalid'>Please Name your project</Form.Control.Feedback>
                    </Form.Group>
                </Row>

                <Row className="mb-3">
                    <Form.Group as={Col} md="8" controlId="validationCustom02">
                        {(addressType == "siteAddress") ? 
                        <><Form.Label className='zoning-form-label'>Street Address</Form.Label>
                        <Form.Control
                        required
                        type="text"
                        placeholder="eg: 747 SW 2nd AVENUE STE 160"
                        value={streetAddress}
                        onChange={(e) => setStreetAddress(e.target.value)}
                        /></> : 
                        <><Form.Label className='zoning-form-label'>PCN</Form.Label>
                        <Form.Control
                        required
                        type="text"
                        placeholder="eg: A-10-30-18-3ZM-000006-00021.0"
                        value={pcn}
                        onChange={(e) => setPCN(e.target.value)}
                        /></>}
                        <Form.Control.Feedback type='invalid'>Please enter Street address</Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} md="4" controlId="validationCustom03">
                        <Form.Label className="ssummary-form-label">City</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="City"
                            value={city}
                            onChange={(e) => setCity(e.target.value)}
                            required
                        />
                        <Form.Control.Feedback type="invalid">
                            Please enter a City.
                        </Form.Control.Feedback>
                    </Form.Group>
                </Row>

                <Row className="mb-3">
                    <Form.Group as={Col} md="6" controlId="validationCustom04">
                        <Form.Label className="ssummary-form-label">County</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="County"
                            value={county}
                            onChange={(e) => setCounty(e.target.value)}
                            required
                        />
                        <Form.Control.Feedback type="invalid">
                            Please provide a County.
                        </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} md="3" controlId="validationCustom05">
                        <Form.Label className="ssummary-form-label">State</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="State"
                            value={state}
                            onChange={(e) => setState(e.target.value)}
                            required
                        />
                        <Form.Control.Feedback type="invalid">
                            Please provide a valid state.
                        </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group as={Col} md="3" controlId="validationCustom06">
                        <Form.Label className="ssummary-form-label">Zip</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Zip"
                            value={zip}
                            onChange={(e) => setZip(e.target.value)}
                            required
                        />
                        <Form.Control.Feedback type="invalid">
                            Please provide a valid zip.
                        </Form.Control.Feedback>
                    </Form.Group>
                </Row>

                <Form.Group className="mb-3">
                    <Form.Label className="ssummary-form-label">Review Type</Form.Label>
                    <Form.Check
                        type='radio'
                        label="Neighbourhood Site Summary"
                        name="Review Type"
                        value="ISiteSummary"
                        onChange={(e) => setReviewType(e.target.value)}
                        required
                    />
                    <Form.Check
                        type='radio'
                        label="Nearby Restaurant Site Summary"
                        name="Review Type"
                        value="NRSiteSummary"
                        onChange={(e) => setReviewType(e.target.value)}
                        required
                    />
                    <Form.Check
                        type='radio'
                        label="Geographic Site Summary"
                        name="Review Type"
                        value="GSiteSummary"
                        onChange={(e) => setReviewType(e.target.value)}
                        required
                    />
                    <Form.Control.Feedback type="invalid">
                        Please choose a Review Type.
                    </Form.Control.Feedback>
                </Form.Group>

                <Form.Group controlId="formSelect" className="mb-3">
                    <Select className="dropdown"
                        placeholder="Choose Buffer Zone"
                        value={bufferdata.filter(obj => selectedBuffers.includes(obj.value))} // set selected values
                        options={bufferdata} // set list of the data
                        onChange={handleChange} // assign onChange function
                        isMulti
                        isClearable
                        required
                    />

                    <Form.Control.Feedback type="invalid">
                        Please select buffer zones.
                    </Form.Control.Feedback>
                </Form.Group>

                <Form.Group md="12" controlId="validationCustom07" className="mb-3">
                    <Form.Label className="ssummary-form-label">Description</Form.Label>
                    <Form.Control
                        type="textarea"
                        placeholder="Your comments..."
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                    />
                </Form.Group>

                <Button type="submit" className="upload-button" >Generate Site Summary Report</Button>
            </Form>
            </Modal.Body>
        </Modal>
        </div>
    )

}

export default SiteSummaryProjects;
