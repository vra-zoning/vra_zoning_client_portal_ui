import React, { useState } from "react";
import "../App.css";
import Footer from "./Footer";
import background from "./images/homebg.jpg";


function DashboardHome() {

  return (
    <>
      <div>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Virtual Review Assist</title>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com"  />
        <link
          href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400;500;700;900&display=swap"
          rel="stylesheet"
        />
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"
        />
        <link rel="stylesheet" href="/css/bootstrap.min.css" />
        <link rel="stylesheet" href="/css/style.css" type="text/css" />
        <div>
        </div>
        <div className="home-hero" style={{ backgroundImage: `url(${background})`, backgroundSize: "cover" }}>
          {/* <video autoPlay muted loop playsInline id="hero-video">
          <source src="images/vra-herovideo.mp4" type="video/mp4" />
        </video> */}
          <div className="container d-flex h-100 col-xl-10 col-xxl-8 px-4 py-5">
            <div className="row align-items-center g-lg-5 py-5">
              <div className="col-lg-10 mt-0 text-center text-lg-start text-white">
                {/* HERO CONTENT */}
                <p className="col-lg-10 fs-2">
                  VRA excels in performing rapid, uniform, comprehensive, and
                  consistent code reviews using state-of-the-art, patent-pending
                  technologies.
                </p>
                {/* END HERO CONTENT */}
              </div>
            </div>
          </div>
        </div>
        {/* END HERO */}
        {/* CONTENT */}
        <div className="container py-5">
          <div className="row align-items-center">
            <div className="col-12 text-center">
              <h2 className="primary-color">Our Partners</h2>
            </div>
          </div>
          <div className="row align-items-center d-flex justify-content-center">
            <div className="col-md-3 py-4">
              <a href="#">
                <img src="images/UF-Logo.png" />
              </a>
            </div>
            <div className="col-md-3 py-4">
              <a href="#">
                <img src="images/Gainsville-Logo.jpg" />
              </a>
            </div>
          </div>
        </div>
        {/* END CONTENT */}
        {/* FOOTER */}
        <Footer />
        {/* END FOOTER */}
      </div>
    </>
  );
}

export default DashboardHome;
