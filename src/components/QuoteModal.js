import { useState, React } from 'react';
import { Modal, Button } from "react-bootstrap";
import { sendQuoteNotification } from './quoteNotification';

const QuoteModal = (props) => {
  const [user, setUser] = useState({ firstName: { value: "", error: true }, lastName: { value: "", error: true }, siteAddress: { value: "", error: true }, phone: { value: "", error: true }, email: { value: "", error: true }, affiliation: { value: "", error: true }, zoning: false, transportation: false, landscape: false, sitesummary: false });
  let invalidFields = true;//to see if all  the fields in the form are filled

  const setValue = (e) => {
    if (!(e.target.value === "")) {
      setUser({ ...user, [e.target.name]: { value: e.target.value, error: false } })
    }
    else {
      setUser({ ...user, [e.target.name]: { value: e.target.value, error: true } })
    }
  };

  const setReview = (e) => {
    if (e.target.name === "zoning") {
      setUser({ ...user, [e.target.name]: !user.zoning })
    }
    if (e.target.name === "transportation") {
      setUser({ ...user, [e.target.name]: !user.transportation })
    }
    if (e.target.name === "landscape") {
      setUser({ ...user, [e.target.name]: !user.landscape })
    }
    if (e.target.name === "sitesummary") {
      setUser({ ...user, [e.target.name]: !user.sitesummary })
    }

    console.log("user", user)

  }

  const getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min);
  }


  const handleQuoteRequest = () => {

    if (user.firstName.error === false && user.lastName.error === false && user.phone.error === false && user.email.error === false && user.affiliation.error === false && (user.zoning === true || user.transportation === true || user.landscape === true || user.sitesummary === true)) {
      invalidFields = false
    }
    console.log(">>>>>", invalidFields)
    console.log("user", user)
    if (invalidFields === false) {

      let templateParams_toTeam = {
        from_name: user.firstName.value,
        to_name: "zoning team",
        subject: "Client requesting a Quote",
        message1: "A client is requesting for a Quote. Following are his details: ",
        message2: "Client Name: " + user.firstName.value + " Contact Email: " + user.email.value + " Contact Phone: " + user.phone.value,
        cc_candidates: "pz@virtualreviewassist.com",
      };

      const TransCode = getRandomInt(10000000000000, 99999999999999);

      let templateParams_toClient = {
        from_name: "VRA",
        to_name: user.firstName.value,
        subject: "Quote Request Confirmation Number",
        message1: "Details of the quote requested for VRA",
        message2: `Confirmation Number for the Quote: ${TransCode}.`,
        cc_candidates: "pz@virtualreviewassist.com" + user.email.value,
      };


      sendQuoteNotification(templateParams_toTeam);
      sendQuoteNotification(templateParams_toClient);

      props.handleClose();
      window.alert("Your request has been sent to the VRA team, they will contact you soon")

    }

  }

  return (
    <Modal show={props.modalOpen} onHide={props.handleClose}>
      <Modal.Header closeButton>
        <Modal.Title><b>Request a Quote</b></Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form>
          <input type="text" className="form-control" name="firstName" placeholder="First Name" onChange={(e) => setValue(e)} />
          {(user.firstName.error === true) && <span className="req-field-msg">***required field</span>}<br /><br />
          <input type="text" className="form-control" name="lastName" placeholder="Last Name" onChange={(e) => setValue(e)} />
          {(user.lastName.error === true) && <span className="req-field-msg">***required field</span>}<br /><br />
          <input type="text" className="form-control" name="phone" placeholder="Phone" onChange={(e) => setValue(e)} />
          {(user.phone.error === true) && <span className="req-field-msg">***required field</span>}<br /><br />
          <input type="text" className="form-control" name="email" placeholder="Email Address" onChange={(e) => setValue(e)} />
          {(user.email.error === true) && <span className="req-field-msg">***required field</span>}<br /><br />
          <input type="text" className="form-control" name="affiliation" placeholder="Affiliation[Company, City, County]" onChange={(e) => setValue(e)} />
          {(user.affiliation.error === true) && <span className="req-field-msg">***required field</span>}<br /><br />
          <input type="text" className="form-control" name="siteAddress" placeholder="Site Address" onChange={(e) => setValue(e)} />
          {(user.siteAddress.error === true) && <span className="req-field-msg">***required field</span>}<br /><br />

          <div className="col-8">
            <h5>Select Your Requiremet</h5>
            {(user.zoning === false && user.transportation === false && user.landscape === false) && <span className="req-field-msg">***required field</span>}
          </div>
          <br />

          <div className="col-8">

            <input value="planning and Zoning" name="zoning" id="one" type="checkbox" onChange={(e) => setReview(e)} />
            <label >Planning and Zoning
            </label>

            <br></br>
            <br></br>

            <input value="Transportation" name="transportation" id="two" type="checkbox" onChange={(e) => setReview(e)} />
            <label >Transportation
            </label>

            <br></br>

            <br></br>

            <input value="Landscape Review" name="landscape" id="three" type="checkbox" onChange={(e) => setReview(e)} />
            <label >Landscape Review
            </label>

            <br></br>

            <br></br>

            <input value="Site Summary" name="sitesummary" id="four" type="checkbox" onChange={(e) => setReview(e)} />
            <label >Site Summary
            </label>

            <br></br>
            <br></br>
          </div>
          <textarea type="text" className="form-control" name="description" placeholder="Description" onChange={(e) => setReview(e)}></textarea><br />
        </form>

      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" style={{ backgroundColor: "#014E6A" }} onClick={handleQuoteRequest} >Submit</Button>
      </Modal.Footer>
    </Modal>
  );
}



export default QuoteModal;
