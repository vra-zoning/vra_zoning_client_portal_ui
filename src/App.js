import React, { useState, useEffect } from "react";
import { Routes, Route } from "react-router-dom";
import LoginPage from "./components/LoginComponents/LoginPage";
import RegisterPage from "./components/LoginComponents/RegisterPage";
import DualAuthentication from "./components/LoginComponents/DualAuth";
import "./App.css";
import Home from "./components/pages/Home";
import DashboardContactUs from "./components/DashboardContactUs";
import DashboardAboutUs from "./components/DashboardAboutUs";
import Dashboard from "./components/Dashboard";
import DashboardHome from "./components/DashboardHome";
import UserProfile from "./components/ViewProfile/UserProfile";
import ResetPassword from "./components/LoginComponents/ResetPassword";
import Navbar from "./components/Navbar";
import IdleTimer from "./IdleTimer";
import SessionTimeoutDialog from "./components/SessionDialogBox";
import VRALiteReportPrint from "./components/VRALiteReportPrint.js";
import axios from "axios";

function App() {
  const API_Endpoint ="https://vrazoningclientws.azurewebsites.net"
  const [isTimeout, setIsTimeout] = useState(false);
  const isAuthenticated = localStorage.getItem("user") && JSON.parse(localStorage.getItem("user"));

  useEffect(() => {
    if (isAuthenticated) {
      if (!isAuthenticated.isAdmin) {
        axios.get(`${API_Endpoint}/api/Admin/CheckifAdmin?email=${isAuthenticated.email}`)
          .then((response) => {
            localStorage.setItem("user", JSON.stringify({ ...isAuthenticated, isAdmin: response.data }))
          })
      }
      const timer = new IdleTimer({
        timeout: 60 * 15, //expire after 15 minutes
        onTimeout: () => {
          setIsTimeout(true);
          setTimeout(logout, 10000);
        },
        onExpired: () => {
          // do something if expired on load
          setIsTimeout(true);
          setTimeout(logout, 10000);
        }
      });
      return () => {
        timer.cleanUp();
      };
    }
  }, [isAuthenticated]);

  const logout = () => {
    setIsTimeout(false);
    localStorage.clear();
    window.location.href = window.location.origin + "/login";
  }

  return (
    <div>{isTimeout ? <div>
      <SessionTimeoutDialog
        countdown={10}
        onContinue={() => {
          setIsTimeout(false);
          window.location.reload(false);
        }}
        onLogout={logout}
        open={true}
      />
    </div> : <>
      <Routes>
        <Route path="/login" element={<LoginPage />} />
        <Route path="/DualAuth" element={<DualAuthentication />} />
        <Route path="/register" element={<RegisterPage />} />
        <Route path="/" exact element={<><Navbar /><Home /></>} />
        <Route path="/DashboardHome" exact element={<><Navbar /><DashboardHome /></>} />
        <Route path="/AboutUs" element={<><Navbar /><DashboardAboutUs /></>} />
        <Route path="/ContactUs" element={<><Navbar /><DashboardContactUs /></>} />
        <Route path="/dashboard" element={<><Navbar /><Dashboard /></>} />
        <Route path="/UserProfile" element={<><Navbar /><UserProfile /></>} />
        <Route path="/reset-password/:token" element={<ResetPassword />} />
        <Route path="/VRALiteReportPrint" element={<VRALiteReportPrint />} />
      </Routes>
    </>}
    </div>
  );
}

export default App;
