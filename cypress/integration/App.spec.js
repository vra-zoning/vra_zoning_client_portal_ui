describe("Ui testing", () =>{
    it('Visits link and check home page loaded', () => {
        cy.visit('http://localhost:3000/')
        Cypress.on('uncaught:exception', (err, runnable) => {
            console.log(err);
            return false;
          })
        cy.get('.home-hero > .container > .row').should('exist')
      })
});
describe("Should route to home page",()=>{
    it("Home page exists",()=>{
        cy.get('.col-12 > .nav > :nth-child(1) > a').click()
        cy.get('.home-hero').should('exist')
    })
});
describe("Should route to your purpose page",()=>{
    it("Home page exists",()=>{
        cy.get('.col-12 > .nav > :nth-child(2) > a').click()
        cy.get('.display-4').should('exist')
    })
});
describe("Should route to Contact Us page",()=>{
    it("Home page exists",()=>{
        cy.get('.col-12 > .nav > :nth-child(3) > a').click()
        cy.get('.display-4').should('exist')
    })
});
describe("login",() =>{
    it('Finding the Login Button and Click on it',()=>{
        cy.get('.text-end > .btn-primary').click()
    })
    it('Enter the email',() =>{
        cy.get('[type="text"]').type('test@test.com')
    })
    it('Enter the password',() =>{
        cy.get('.password-block-login').type('ssvra')
        cy.get('.each-input > .app-btn').click()
    })
    
});
// describe("Testing the user Profile",()=>{
//     it('Route to user Profile',()=>{
//         cy.get(':nth-child(2) > .card-link').click()
//         cy.get(':nth-child(1) > .left-menu').should('exist')
//     })
//     // it('Should Router to Date Report Page',()=>{
//     //     cy.get(':nth-child(2) > .left-menu').click()
//     //     // cy.wait(5000)
//     // })
// })

describe("Start Filling the form",()=>{
    it('New project card should be visible',() =>{
        cy.get('.col-12>.nav>:nth-child(3) > a').click()
        cy.wait(5000)
        Cypress.on('uncaught:exception', (err, runnable) => {
            console.log(err);
            return false;
          })
        cy.get('.new-project-card').should('be.visible')
        cy.get('.new-project-card').click()
        cy.get(':nth-child(1) > form > .form-group > [name="projectName"]').type('test_by_ravi')
        cy.get(':nth-child(4) > [placeholder="Street Address"]').type('4000 SW 37TH BLVD')
        cy.get(':nth-child(4) > [name="cityName"]').type('Gainesville')
        cy.get(':nth-child(4) > [name="countyName"]').type('Alachua')
        cy.get(':nth-child(4) > [placeholder="State"]').type('Florida')
        cy.get(':nth-child(4) > [placeholder="Zipcode"]').type('32608')
        cy.get(':nth-child(1) > form > .form-group > .form-check > #SiteSummary').click()
        // cy.get('.css-6j8wv5-Input').click()
        // cy.get('.css-12jo7m5').click()
        // cy.get('.css-12jo7m5').select("1 mi")
        cy.get(':nth-child(1) > form > .form-group > textarea.form-control').type('Cypress Testing')
        // cy.get(':nth-child(1) > form > .form-group > .btn').click()
        cy.get('.btn-close').click()
    })
});
// describe("Testing the user Profile",()=>{
//     it('Route to user Profile',()=>{
//         cy.get(':nth-child(2) > .card-link').click()
//         cy.get(':nth-child(1) > .left-menu').should('exist')
//     })
//     // it('Should Router to Date Report Page',()=>{
//     //     cy.get(':nth-child(2) > .left-menu').click()
//     //     // cy.wait(5000)
//     // })
// })

